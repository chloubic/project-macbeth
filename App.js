import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import React, {useEffect, useState} from 'react';
import {StatusBar} from 'react-native';
import Settings from './src/pages/Settings';
import Profile from './src/pages/Profile';
import {NavigationContainer} from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import RestaurantNavigation from './src/pages/RestaurantList/navigation';
import {useNetInfo} from '@react-native-community/netinfo';
import {ErrorText} from './src/components/Common/styled';

const Tab = createMaterialBottomTabNavigator();

const App: () => React$Node = () => {
    const netInfo = useNetInfo();
    return (
        <>
            <StatusBar backgroundColor={'#ffb200'} barStyle="light-content"/>
            {netInfo.isConnected && <NavigationContainer>
                <Tab.Navigator
                    initialRouteName="Restaurants"
                    activeColor="#ffb200"
                    shifting={true}
                    barStyle={{backgroundColor: 'white'}}
                >
                    <Tab.Screen
                        name="Restaurants"
                        component={RestaurantNavigation}
                        options={{
                            tabBarLabel: 'Restaurace',
                            tabBarIcon: ({color}) => (
                                <FontAwesome5 name={'utensils'} size={20} color={color} solid/>
                            ),
                        }}
                    />
                    <Tab.Screen
                        name="Profile"
                        component={Profile}
                        options={{
                            tabBarLabel: 'Profil',
                            tabBarIcon: ({color}) => (
                                <FontAwesome5 name={'user'} size={20} color={color} solid/>
                            ),
                        }}
                    />
                    <Tab.Screen
                        name="Settings"
                        component={Settings}
                        options={{
                            tabBarLabel: 'O aplikaci',
                            tabBarIcon: ({color}) => (
                                <FontAwesome5 name={'cog'} size={20} color={color} solid/>
                            ),
                        }}
                    />
                </Tab.Navigator>
            </NavigationContainer>}
            {!netInfo.isConnected && <ErrorText>Zkontrolujte své připojení k internetu</ErrorText>}
        </>
    );
};

export default App;
