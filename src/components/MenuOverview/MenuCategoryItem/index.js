import React from 'react';
import {MenuCategoryName, MenuCategoryTouchable} from '../styled';


const MenuAdminCategoryItem: () => React$Node = ({ navigation, id, name }) => {
    return (
        <MenuCategoryTouchable onPress={() => navigation.navigate("MenuDetail", { id: id })}>
            <MenuCategoryName>{name}</MenuCategoryName>
        </MenuCategoryTouchable>
    );
};

export default MenuAdminCategoryItem;
