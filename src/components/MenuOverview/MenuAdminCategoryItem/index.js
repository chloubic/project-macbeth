import React from 'react';
import {MenuCategoryName, MenuCategoryTouchable} from '../styled';


const MenuCategoryItem: () => React$Node = ({ navigation, id, name }) => {
    return (
        <MenuCategoryTouchable onPress={() => navigation.navigate("MenuAdminDetail", { id: id })}>
            <MenuCategoryName>{name}</MenuCategoryName>
        </MenuCategoryTouchable>
    );
};

export default MenuCategoryItem;
