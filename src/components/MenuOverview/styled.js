import React from 'react';
import styled from 'styled-components/native';

export const MenuList = styled.ScrollView`
`;

export const MenuTitle = styled.Text`
    padding: 20px 20px 10px 20px;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 16px;
    color: #666;
`;

export const MenuCategoryTouchable = styled.TouchableOpacity`
    background-color: white;
    color: white;
    margin: 10px 20px;
    border-radius: 4px;
    shadow-color: red;
    shadow-offset: 0 0;
    shadow-opacity: 0.8;
    shadow-radius: 5px;
    elevation: 2;
    padding: 20px;
    position: relative;
`;

export const MenuCategoryName = styled.Text`
    color: #222222;
    font-size: 20px;
`;
