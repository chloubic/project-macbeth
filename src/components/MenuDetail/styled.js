import React from 'react';
import styled from 'styled-components/native';

export const MenuView = styled.View`
    background-color: white;
    color: white;
    margin: 10px 20px;
    border-radius: 4px;
    shadow-color: red;
    shadow-offset: 0 0;
    shadow-opacity: 0.8;
    shadow-radius: 5px;
    elevation: 2;
    padding: 20px;
    position: relative;
`;

export const MenuAdminView = styled.TouchableOpacity`
    background-color: white;
    color: white;
    margin: 10px 20px;
    border-radius: 4px;
    shadow-color: red;
    shadow-offset: 0 0;
    shadow-opacity: 0.8;
    shadow-radius: 5px;
    elevation: 2;
    padding: 20px;
    position: relative;
`;

export const MenuName = styled.Text`
`;

export const MenuDescription = styled.Text`
`;

export const MenuWeight = styled.Text`
`;

export const MenuPrice = styled.Text`
`;
