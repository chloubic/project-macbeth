import React from 'react';
import {MenuDescription, MenuName, MenuPrice, MenuView, MenuWeight} from '../styled';

const MenuItem: () => React$Node = ({ name, description, price, weight }) => {
    return (
        <MenuView>
            <MenuName>{name}</MenuName>
            <MenuDescription>{description}</MenuDescription>
            <MenuWeight>{weight}</MenuWeight>
            <MenuPrice>{price}</MenuPrice>
        </MenuView>
    );
};

export default MenuItem;
