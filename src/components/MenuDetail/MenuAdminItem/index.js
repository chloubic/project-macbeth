import React from 'react';
import {MenuAdminView, MenuDescription, MenuName, MenuPrice, MenuWeight} from '../styled';

const MenuAdminItem: () => React$Node = ({ navigation, id, name, description, price, weight }) => {
    return (
        <MenuAdminView onPress={() => navigation.navigate("MenuAdminDetailInfo", { id: id })}>
            <MenuName>{name}</MenuName>
            <MenuDescription>{description}</MenuDescription>
            <MenuWeight>{weight}</MenuWeight>
            <MenuPrice>{price}</MenuPrice>
        </MenuAdminView>
    );
};

export default MenuAdminItem;
