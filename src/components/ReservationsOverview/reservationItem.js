import {Text, View} from 'react-native';
import {BackButton} from '../Form/styled';
import React, {useCallback, useState} from 'react';
import {
    ReservationDateTime,
    ReservationEmail,
    ReservationName, ReservationPhone,
    ReservationTable,
    ReservationUserTitle,
    ReservationView,
} from './styled';
import Moment from 'react-moment';
import {useFocusEffect} from '@react-navigation/native';

const ReservationItem: () => React$Node = ({ userName, userEmail, userPhone, table, date, from, to, withDetail = true, cancelReservation = null, id = null }) => {
    const [show, setShow] = useState(false);

    useFocusEffect(
        useCallback(() => {
            setShow(false);
        }, []),
    );
    return (
        <ReservationView>
            <ReservationTable>{table}</ReservationTable>
            <ReservationDateTime><Moment element={Text} format={'Do MMMM YYYY'} date={date}/> <Moment element={Text} format={'H:mm'} date={from}/> - <Moment element={Text} format={'H:mm'} date={to}/></ReservationDateTime>
            {withDetail && !show && <BackButton mode={'text'} labelStyle={{color: '#ffb200', paddingTop: 10, paddingBottom: 10}} onPress={() => setShow(true)}>Zobrazit detail</BackButton>}
            {!withDetail && <BackButton mode={'text'} labelStyle={{color: '#ffb200', paddingTop: 10, paddingBottom: 10}} onPress={() => cancelReservation(id)}>Zrušit rezervaci</BackButton>}
            {show && <View>
                <ReservationUserTitle>Zákazník</ReservationUserTitle>
                <ReservationName>{userName}</ReservationName>
                <ReservationEmail>{userEmail}</ReservationEmail>
                <ReservationPhone>{userPhone}</ReservationPhone>
                <BackButton mode={'text'} labelStyle={{color: '#ffb200', paddingBottom: 10}} onPress={() => setShow(false)}>Skrýt detail</BackButton>
            </View>}
        </ReservationView>
    );
};

export default ReservationItem;
