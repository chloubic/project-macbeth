import React from 'react';
import styled from 'styled-components/native';
import {Divider} from 'react-native-elements';

export const ReservationView = styled.View`
    background-color: white;
    color: white;
    margin: 10px 20px;
    border-radius: 4px;
    shadow-color: red;
    shadow-offset: 0 0;
    shadow-opacity: 0.8;
    shadow-radius: 5px;
    elevation: 2;
    position: relative;
`;

export const ReservationTable = styled.Text`
    padding: 20px 20px 0 20px;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 16px;
    color: #666;
`;

export const ReservationDateTime = styled.Text`
    padding: 0 20px;
    text-transform: uppercase;
    font-weight: 500;
    font-size: 16px;
    color: #666;
`;

export const ReservationUserTitle = styled.Text`
    padding: 20px 20px 0 20px;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 16px;
    color: #ffb200;
`;

export const ReservationName = styled.Text`
    padding: 0 20px;
    font-weight: bold;
    font-size: 16px;
    color: #666;
`;

export const ReservationEmail = styled.Text`
    padding: 0 20px
    font-weight: 500;
    font-size: 16px;
    color: #666;
`;

export const ReservationPhone = styled.Text`
    padding: 0 20px;
    font-weight: 400;
    font-size: 16px;
    color: #666;
`;

export const ReservationTitle = styled.Text`
    padding: 20px;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 16px;
    color: #666;
`;

export const DateTimePickerView = styled.View`
    margin: 0 20px;
`;

export const ReservationDivider = styled(Divider)`
    margin: 35px 20px 25px 20px;
    background-color: #aaa;
`;
