import React from 'react';
import styled from 'styled-components/native';

export const SettingsImage = styled.Image`
    width: 100%;
    height: 150px;
    resize-mode: contain;
    margin-bottom: 50px;
    margin-top: 50px;
`;

export const PurposeText = styled.Text`
    width: 100%;
    padding: 0 30px;
    text-align: center;
    font-size: 16px;
    font-weight: 400;
    line-height: 25px;
    color: #222222;
`;

export const CopyrightText = styled.Text`
    width: 100%;
    padding: 0 30px;
    text-align: center;
    font-size: 16px;
    font-weight: 400;
    line-height: 25px;
    color: #222;
    margin-top: 50px;
    color: #222222;
`;

export const AuthorText = styled.Text`
    width: 100%;
    padding: 10px 30px;
    text-align: center;
    font-size: 18px;
    font-weight: 700;
    line-height: 25px;
    color: #ffb200;
`;
