import React from 'react';
import styled from 'styled-components/native';

export const ErrorText = styled.Text`
    padding: 20px;
    font-weight: 500;
    font-size: 18px;
    color: #666666;
`;

export const LoaderView = styled.View`
    width: 100%;
    height: 100%;
    position: absolute;
`;

export const LoaderImage = styled.Image`
    width: 100%;
    height: 100px;
    resize-mode: contain;
    margin-bottom: 50px;
    margin-top: 20px;
`;

export const LoadingText = styled.Text`
    text-align: center;
    font-size: 18px;
    font-weight: 400;
    color: #666666;
`;
