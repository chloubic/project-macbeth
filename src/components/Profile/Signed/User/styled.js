import React from 'react';
import styled from 'styled-components/native';

export const ProfileImage = styled.Image`
    width: 200px;
    height: 200px;
    resize-mode: cover;
    border-radius: 200px;
    justify-content: center;
    margin: 50px auto;
`;

export const ProfileFormView = styled.View`
    padding: 0 20px 30px 20px;
`;
