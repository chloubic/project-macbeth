import React from 'react';
import {BackButton} from '../Form/styled';
import {Alert, ToastAndroid} from 'react-native';
import {PhotoImage, PhotoTouchable} from './styled';
import updatePhotogallery from '../../api/Photogallery/updatePhotogallery';

const PhotoItem: () => React$Node = ({ id, source, photos, setPhotos }) => {
    const removePhoto = () => {
        let arr = photos.filter(item => item !== source);
        updatePhotogallery({ id: id, photos: arr }).then(result => {
            if(result.status === 200){
                ToastAndroid.showWithGravityAndOffset(
                    'Obrázek odebrán',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                );
                setPhotos(arr);
            }
            else {}
        }).catch();
    };

    return (
        <PhotoTouchable>
            <PhotoImage
                source={{uri: source}}
            />
            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                Alert.alert(
                    'Smazání obrázku',
                    'Odstranění obrázku je nezvratný proces!',
                    [
                        {
                            text: 'Zrušit',
                            style: 'cancel',
                        },
                        {
                            text: 'Smazat', style: '', onPress: () => removePhoto(),
                        },
                    ],
                    {cancelable: false},
                );
            }}>Smazat obrázek</BackButton>
        </PhotoTouchable>
    );
};

export default PhotoItem;
