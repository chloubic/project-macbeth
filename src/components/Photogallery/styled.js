import React from 'react';
import styled from 'styled-components/native';
import {TouchableOpacity} from 'react-native';

export const PhotoList = styled.ScrollView`
    margin-bottom: 60px;
`;

export const LabelText = styled.Text`
    padding: 0 0 10px 0;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 16px;
    color: #666;
`;

export const DayText = styled.Text`
    padding: 10px 20px 10px 20px;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 16px;
    color: #666;
    text-align: center;
`;

export const PhotoTouchable = styled(TouchableOpacity)`
    background-color: white;
    color: white;
    margin: 10px 20px;
    border-radius: 4px;
    shadow-color: red;
    shadow-offset: 0 0;
    shadow-opacity: 0.8;
    shadow-radius: 5px;
    elevation: 2;
    position: relative;
`;

export const PhotoImage = styled.Image`
    width: 100%;
    height: 150px;
    shadow-radius: 5px;
`;
