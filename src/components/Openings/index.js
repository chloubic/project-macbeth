import {DayText, PhotoTouchable} from '../Photogallery/styled';
import {BackButton} from '../Form/styled';
import {Alert, Text, ToastAndroid} from 'react-native';
import React from 'react';
import updateOpenings from '../../api/Openings/updateOpenings';
import Moment from 'react-moment';

const OpeningItem: () => React$Node = ({ id, index, dayId, from, to, openings, setOpenings, days }) => {
    const removeOpening = () => {
        let arr = openings.filter((item, ind) => ind !== index);
        updateOpenings({ id: id, openings: arr }).then(result => {
            if(result.status === 200){
                setOpenings(arr);
                ToastAndroid.showWithGravityAndOffset(
                    'Otevírací doba odebrána',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                );
            }
            else {}
        }).catch();
    };
    return (
        <PhotoTouchable>
            <DayText>{days.filter(item => item.id === dayId)[0].name}: <Moment element={Text} format={'H:mm'} date={from}/> - <Moment element={Text} format={'H:mm'} date={to}/></DayText>
            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                Alert.alert(
                    'Smazání otevírací doby',
                    'Odstranění položky je nezvratný proces!',
                    [
                        {
                            text: 'Zrušit',
                            style: 'cancel',
                        },
                        {
                            text: 'Smazat', style: '', onPress: () => removeOpening(),
                        },
                    ],
                    {cancelable: false},
                );
            }}>Smazat položku</BackButton>
        </PhotoTouchable>
    );
};

export default OpeningItem;
