import React from 'react';
import styled from 'styled-components/native';


export const RestaurantDetailImage = styled.Image`
    width: 100%;
    height: 200px;
`;

export const RestaurantDetailName = styled.Text`
    width: 100%;
    font-size: 20px;
    font-weight: bold;
    color: #222222;
    padding: 20px;
`;

export const RestaurantDetailAddress = styled.Text`
    width: 100%;
    font-size: 18px;
    color: #222222;
    padding: 0 20px;
`;

export const RestaurantDetailStatusOpened = styled.Text`
    width: 100%;
    font-size: 16px;
    font-weight: bold;
    color: green;
    padding: 10px 20px 15px 20px;
`;

export const RestaurantDetailStatusClosed = styled(RestaurantDetailStatusOpened)`
    color: red;
`;

export const RestaurantDetailDescription = styled.Text`
    width: 100%;
    font-size: 22px;
    font-weight: 400;
    color: #888888;
    padding: 10px 20px;
`;

export const RestaurantDetailButtonView = styled.View`
    width: 100%;
    padding: 10px;
`;

export const RestaurantDetailOpenings = styled.View`
    
`;

export const RestaurantDetailOpening = styled.Text`
    padding: 5px 20px;
    font-size: 18px;
`;
