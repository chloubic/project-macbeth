import React from 'react';
import styled from 'styled-components/native';
import {TouchableOpacity} from 'react-native';


export const CardTouchable = styled(TouchableOpacity)`
    background-color: white;
    color: white;
    margin: 10px 20px;
    border-radius: 4px;
    shadow-color: red;
    shadow-offset: 0 0;
    shadow-opacity: 0.8;
    shadow-radius: 5px;
    elevation: 2;
    position: relative;
`;

export const CardImage = styled.Image`
    width: 100%;
    height: 125px;
    border-radius: 4px;
`;

export const CardName = styled.Text`
    font-size: 18px;
    font-weight: bold;
    color: #222222;
    padding: 10px 20px;
`;

export const CardAddress = styled.Text`
    font-size: 16px;
    color: #222222;
    padding: 0 20px;
`;

export const CardStatusOpened = styled.Text`
    font-size: 16px;
    font-weight: bold;
    color: green;
    padding: 5px 20px 15px 20px;
`;

export const CardStatusClosed = styled(CardStatusOpened)`
    color: red;
`;
