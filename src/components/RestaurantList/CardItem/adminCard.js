import React from 'react';
import {
    CardAddress,
    CardImage,
    CardName,
    CardStatusClosed,
    CardStatusOpened,
    CardTouchable,
} from './styled';

const RestaurantAdminCard: () => React$Node = ({navigation, id, name, address, image, status}) => {
    return (
        <CardTouchable onPress={() => navigation.navigate("RestaurantAdminInfo", { id: id })}>
            <CardImage
                source={{uri: image}}
            />
            <CardName>{name}</CardName>
            <CardAddress>{address}</CardAddress>
            {status && <CardStatusOpened>Otevřeno</CardStatusOpened>}
            {!status && <CardStatusClosed>Zavřeno</CardStatusClosed>}
        </CardTouchable>
    );
};

export default RestaurantAdminCard;

