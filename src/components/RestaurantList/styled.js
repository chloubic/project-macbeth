import React from 'react';
import styled from 'styled-components/native';


export const RestaurantListView = styled.ScrollView`
    margin: 10px 0 60px 0;
`;

export const RestaurantImage = styled.Image`
    width: 100%;
    height: 150px;
`;
