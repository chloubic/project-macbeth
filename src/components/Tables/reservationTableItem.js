import React from 'react';
import {DayText, PhotoTouchable} from '../Photogallery/styled';
import {BackButton} from '../Form/styled';
import Roles from '../../api/Auth/Roles';

const ReservationTableItem: () => React$Node = ({id, name, newReservation, role }) => {
    return (
        <PhotoTouchable>
            <DayText>{name}</DayText>
            {role != null && role === Roles.Client && <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => newReservation(id)}>Zarezervovat</BackButton>}
        </PhotoTouchable>
    );
};

export default ReservationTableItem;

