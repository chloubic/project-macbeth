import React from 'react';
import {DayText, PhotoTouchable} from '../Photogallery/styled';
import {BackButton} from '../Form/styled';
import {Alert, ToastAndroid} from 'react-native';
import deleteTable from '../../api/Tables/deleteTable';

const TableItem: () => React$Node = ({id, name, tables, setTables}) => {
    const removeTable = () => {
        let arr = tables.filter(item => item.id !== id);
        deleteTable({ id: id }).then(result => {
            if(result.status === 200){
                setTables(arr);
                ToastAndroid.showWithGravityAndOffset(
                    'Stůl k sezení odebrán',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                );
            }
            else {}
        }).catch();
    };
    return (
        <PhotoTouchable>
            <DayText>{name}</DayText>
            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                Alert.alert(
                    'Smazání stolu k sezení',
                    'Odstranění položky je nezvratný proces!',
                    [
                        {
                            text: 'Zrušit',
                            style: 'cancel',
                        },
                        {
                            text: 'Smazat', style: '', onPress: () => removeTable(),
                        },
                    ],
                    {cancelable: false},
                );
            }}>Smazat položku</BackButton>
        </PhotoTouchable>
    );
};

export default TableItem;

