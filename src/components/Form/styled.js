import React from 'react';
import styled from 'styled-components/native';
import {TextInput, Button} from 'react-native-paper';

export const FormImage = styled.Image`
    width: 100%;
    height: 150px;
    resize-mode: contain;
    margin-bottom: 50px;
`;

export const FormView = styled.ScrollView`
    padding: 30px;
    flex: 1;
`;

export const InputView = styled.View`
    margin: 0 0 30px 0;
`;

export const RegistrationLabel = styled.Text`
    text-transform: uppercase;
    font-weight: 700;
    font-size: 18px;
    text-align: center;
    color: #ffb200;
    margin-bottom: 50px;
`;

export const FormLabel = styled.Text`
    text-transform: uppercase;
    font-weight: 500;
    color: #222222;
`;

export const FormTextInput = styled(TextInput)`
    background-color: #f8f8f8;    
`;

export const BackButton = styled(Button)`
`;

export const ButtonView = styled.View`
    padding: 20px 0;
`;

export const ValidationText = styled.Text`
    color: #FF6F00;
    margin-bottom: 5px;
    margin-top: 5px;
`;
