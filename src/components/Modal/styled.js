import styled from 'styled-components/native/dist/styled-components.native.esm';

export const ModalView = styled.ScrollView`
    flex: 1;
    background-color: white;
    border-radius: 4px;
`;

export const ModalHeader = styled.Text`
    padding: 20px 20px 20px 20px;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 16px;
    color: #666;
`;

export const OperatorsList = styled.View`
    padding: 20px 0;
`;

export const OperatorView = styled.View`
    background-color: white;
    color: white;
    margin: 10px 20px;
    border-radius: 4px;
    shadow-color: red;
    shadow-offset: 0 0;
    shadow-opacity: 0.8;
    shadow-radius: 5px;
    elevation: 2;
    padding: 15px 20px 0 20px;
    position: relative;
`;

export const OperatorName = styled.Text`
    color: #222222;
    font-size: 20px;
`;

export const OperatorEmail = styled.Text`
    color: #222222;
    font-size: 16px;
`;

export const OperatorPhone = styled.Text`
    color: #222222;
    font-size: 16px;
`;
