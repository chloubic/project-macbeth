import React from 'react';
import {Linking, SafeAreaView, ScrollView, Text, TouchableOpacity} from 'react-native';
import {FormImage} from '../../components/Common/styled';
import {FormView} from '../../components/Form/styled';
import {AuthorText, CopyrightText, PurposeText, SettingsImage} from '../../components/Settings/styled';

const Settings: () => React$Node = () => {
    return (
        <SafeAreaView>
            <ScrollView>
                <SettingsImage source={require('../../../resources/logo.png')}/>
                <PurposeText>Tato aplikace byla vytvořena v rámci semestrální práce z předmětu KIV/MBKZ.</PurposeText>
                <CopyrightText>Copyright &copy; 2020</CopyrightText>
                <TouchableOpacity onPress={() => Linking.openURL('https://dchlouba.cz')}>
                    <AuthorText>
                        Vytvořil Dominik Chlouba
                    </AuthorText>
                </TouchableOpacity>
            </ScrollView>
        </SafeAreaView>
    );
};

export default Settings;

