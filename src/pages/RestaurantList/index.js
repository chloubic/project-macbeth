import React, {useCallback, useEffect, useState} from 'react';
import {Alert, SafeAreaView, Text} from 'react-native';
import RestaurantCard from '../../components/RestaurantList/CardItem';
import {RestaurantListView} from '../../components/RestaurantList/styled';
import {useFocusEffect} from '@react-navigation/native';
import getRestaurants from '../../api/Restaurants/getRestaurants';
import {MenuTitle} from '../../components/MenuOverview/styled';
import Error from '../../api';
import {ErrorText} from '../../components/Common/styled';
import Loader from '../../components/Common/loader';

const RestaurantList: () => React$Node = ({navigation}) => {
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState({
        list: null,
        page: 1,
        limit: 200,
        total: 2,
    });

    async function fetchRestaurants() {
        getRestaurants({page: data.page, descOrder: true, search: search}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setData({
                        ...data,
                        list: json.data,
                        total: json.total,
                    });
                    setLoading(false);
                }).catch(() => Error());
            } else {
                Error();
            }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            if(data.list == null) setLoading(true);
            setSearch('');
            fetchRestaurants().catch(() => Error());
        }, []),
    );
    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Přehled restaurací</MenuTitle>
                <RestaurantListView contentInsetAdjustmentBehavior="automatic">
                    {data.list != null && data.list.map(item => {
                        return <RestaurantCard navigation={navigation} key={item.id} id={item.id} name={item.name}
                                               address={item.address}
                                               image={item.mainImage} status={item.status}/>;
                    })}
                    {data.list != null && data.list.length === 0 &&
                    <ErrorText>Žádné restaurace nebyly nalezeny</ErrorText>}
                </RestaurantListView>
            </SafeAreaView>
        </>
    );
};

export default RestaurantList;

