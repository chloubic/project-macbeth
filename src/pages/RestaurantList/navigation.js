import React, {useCallback} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import RestaurantDetail from '../RestaurantDetail';
import RestaurantList from './index';
import RestaurantReservationOverview from '../RestaurantDetail/ReservationOverview';
import RestaurantGallery from '../RestaurantDetail/RestaurantGallery';
import MenuOverview from '../RestaurantDetail/MenuOverview';
import MenuDetail from '../RestaurantDetail/MenuDetail';

let Stack = createStackNavigator();

const RestaurantNavigation: () => React$Node = () => {
    return (
        <Stack.Navigator headerMode={"none"} initialRouteName={"RestaurantList"}>
            <Stack.Screen name="RestaurantList" component={RestaurantList}/>
            <Stack.Screen name="RestaurantDetail" component={RestaurantDetail}/>
            <Stack.Screen name="RestaurantGallery" component={RestaurantGallery}/>
            <Stack.Screen name="RestaurantReservationOverview" component={RestaurantReservationOverview}/>
            <Stack.Screen name="MenuOverview" component={MenuOverview}/>
            <Stack.Screen name="MenuDetail" component={MenuDetail}/>
        </Stack.Navigator>
    );
};

export default RestaurantNavigation;

