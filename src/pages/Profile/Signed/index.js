import React, {useCallback, useState} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Identity from '../../../api/Auth/Identity';
import User from './User';
import Roles from '../../../api/Auth/Roles';
import Reservations from './Reservations';
import Operators from './Operators';
import RestaurantAdminNavigation from './Restaurants/navigation';
import Loader from '../../../components/Common/loader';

const Tab = createMaterialTopTabNavigator();

const Signed: () => React$Node = ({ navigation }) => {
    const [ role, setRole ] = useState(null);
    const [loading, setLoading] = useState(true);
    useFocusEffect(
        useCallback(() => {
            if(role == null) setLoading(true);
            Identity().then(result => {
                if(result.isValid){
                    setLoading(false);
                    setRole(result.role);
                }
                else AsyncStorage.removeItem("token").then(res => {
                    navigation.navigate("Unsigned");
                }).catch(() => navigation.navigate("Unsigned"));
            }).catch(() => navigation.navigate("Unsigned"));
        }, [])
    );
    return (
        <>
            <Loader visible={loading}/>
            {role != null && <Tab.Navigator
                tabBarOptions={{
                    activeTintColor: '#ffffff',
                    labelStyle: {fontSize: 14, fontWeight: 'bold'},
                    indicatorStyle: {
                        backgroundColor: 'white',
                    },
                    style: {backgroundColor: '#ffb200', color: 'white'},
                }}>
                <Tab.Screen name="User" component={User} options={{tabBarLabel: 'Uživatel'}}/>
                {role != null && role === Roles.Client && <Tab.Screen name="Reservations" component={Reservations} options={{tabBarLabel: 'Rezervace'}}/>}
                {role != null && role === Roles.Admin && <Tab.Screen name="Operators" component={Operators} options={{tabBarLabel: 'Operátoři'}}/>}
                {role != null && (role === Roles.Admin || role === Roles.Operator) && <Tab.Screen name="RestaurantList" component={RestaurantAdminNavigation} options={{tabBarLabel: 'Restaurace'}}/>}
            </Tab.Navigator>}
        </>
    );
};

export default Signed;

