import React, {useCallback, useState} from 'react';
import {SafeAreaView} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {MenuTitle} from '../../../../components/MenuOverview/styled';
import {PhotoList} from '../../../../components/Photogallery/styled';
import ReservationItem from '../../../../components/ReservationsOverview/reservationItem';
import getClientReservations from '../../../../api/Reservations/getClientReservations';
import deleteReservation from '../../../../api/Reservations/deleteReservation';
import Identity from '../../../../api/Auth/Identity';
import AsyncStorage from '@react-native-community/async-storage';
import Error from '../../../../api';
import {ErrorText} from '../../../../components/Common/styled';
import Loader from '../../../../components/Common/loader';

const Reservations: () => React$Node = () => {
    const [reservations, setReservations] = useState(null);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);

    async function fetchReservations() {
        getClientReservations().then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setLoading(false);
                    setReservations(json);
                }).catch(() => Error());
            } else {
            }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    fetchReservations().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate('Unsigned');
                    }).catch(() => navigation.navigate('Unsigned'));
                }
            }).catch(() => navigation.navigate('Unsigned'));
        }, []),
    );

    const cancelReservation = (id) => {
        deleteReservation({id: id}).then(result => {
            if (result.status === 200) {
                fetchReservations().catch(() => Error());
            } else {
                Error();
            }
        }).catch(() => Error());
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Mé rezervace</MenuTitle>
                <PhotoList>
                    {reservations == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {reservations != null && reservations.length === 0 &&
                    <ErrorText>Nenalezeny žádné rezervace</ErrorText>}
                    {reservations != null && reservations.map(item => {
                        return <ReservationItem key={item.id}
                                                userName={item.userName}
                                                userEmail={item.userEmail}
                                                userPhone={item.userPhone}
                                                table={item.table}
                                                date={item.date}
                                                from={item.from}
                                                to={item.to}
                                                withDetail={false}
                                                id={item.id}
                                                cancelReservation={cancelReservation}
                        />;
                    })}
                </PhotoList>
            </SafeAreaView>
        </>
    );
};

export default Reservations;

