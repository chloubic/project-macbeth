import React, {useCallback, useState} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {Alert, SafeAreaView, ScrollView, Text, ToastAndroid, View} from 'react-native';
import {ProfileFormView, ProfileImage} from '../../../../components/Profile/Signed/User/styled';
import {
    BackButton, ButtonView,
    FormTextInput,
    InputView,
    ValidationText,
} from '../../../../components/Form/styled';
import {Button} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Identity from '../../../../api/Auth/Identity';
import Roles from '../../../../api/Auth/Roles';
import deleteMyself from '../../../../api/Auth/deleteMyself';
import getOrganizationInfo from '../../../../api/Info/getOrganizationInfo';
import getUserInfo from '../../../../api/Info/getUserInfo';
import saveOrganizationInfo from '../../../../api/Info/saveOrganizationInfo';
import saveUserInfo from '../../../../api/Info/saveUserInfo';
import changePassword from '../../../../api/Info/changePassword';
import Error from '../../../../api';
import Loader from '../../../../components/Common/loader';

const REG = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const DEFAULT_FORM_VALS = {
    username: {
        value: '',
        isValid: false,
    },
    email: {
        value: '',
        isValid: false,
    },
    phone: {
        value: '',
        isValid: false,
    },
    password: {
        value: '',
        isValid: true,
    },
    submitted: false,
};

const DEFAULT_ORG_VALS = {
    name: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const User: () => React$Node = ({navigation, route}) => {
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [org, setOrg] = useState(DEFAULT_ORG_VALS);
    const [role, setRole] = useState(null);
    const [loading, setLoading] = useState(true);

    const fetchUserInfo = async () => {
        getUserInfo().then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setForm({
                        username: {
                            value: json.name,
                            isValid: true,
                        },
                        email: {
                            value: json.email,
                            isValid: true,
                        },
                        phone: {
                            value: json.phoneNumber,
                            isValid: true,
                        },
                        password: {
                            value: '',
                            isValid: true,
                        },
                        submitted: false,
                    });
                    setLoading(false);
                }).catch(() => Error());
            } else {
                Error();
            }
        }).catch(() => Error());
    };

    const fetchOrgInfo = async () => {
        getOrganizationInfo().then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setOrg({
                        name: {
                            value: json.name,
                            isValid: false,
                        },
                        submitted: false,
                    });
                }).catch(() => Error());
            } else {
                Error();
            }
        }).catch(() => Error());
    };

    useFocusEffect(
        useCallback(() => {
            if(role == null) setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    setRole(result.role);
                    fetchUserInfo().catch(() => Error());
                    if(result.role === Roles.Admin) fetchOrgInfo().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate('Unsigned');
                    }).catch(() => navigation.navigate('Unsigned'));
                }
            }).catch(() => navigation.navigate('Unsigned'));
        }, []),
    );
    const submit = () => {
        setForm({
            ...form,
            submitted: true,
        });
        if (form.username.isValid && form.email.isValid && form.phone.isValid && form.password.isValid) {
            saveUserInfo({name: form.username.value, email: form.email.value, phone: form.phone.value}).then(result => {
                if (result.status === 200) {
                    if (form.password.value !== '') {
                        changePassword({password: form.password.value}).then(res => {
                            if (res.status === 200) {
                                fetchUserInfo().catch(() => Error());
                                ToastAndroid.showWithGravityAndOffset(
                                    'Informace aktualizovány',
                                    ToastAndroid.LONG,
                                    ToastAndroid.BOTTOM,
                                    25,
                                    50,
                                );
                            } else {
                                fetchUserInfo().catch(() => Error());
                                ToastAndroid.showWithGravityAndOffset(
                                    'Informace aktualizovány',
                                    ToastAndroid.LONG,
                                    ToastAndroid.BOTTOM,
                                    25,
                                    50,
                                );
                                Alert.alert(
                                    'Aktualizace informací',
                                    'Informace aktualizovány, nicméně heslo nebylo změněno!',
                                    [{text: 'OK', style: 'cancel'}],
                                    {cancelable: false},
                                );
                            }
                        }).catch(() => Error());
                    } else {
                        fetchUserInfo().catch(() => Error());
                        ToastAndroid.showWithGravityAndOffset(
                            'Informace aktualizovány',
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50,
                        );
                    }
                } else {
                    Error();
                }
            }).catch(() => Error());
        }
    };

    const removeMyself = () => {
        if (role === Roles.Admin) {
            Alert.alert(
                'Smazání mého účtu',
                'Odstraněním svého účtu rovněž smažete veškeré informace spojené s organizací!',
                [
                    {
                        text: 'Zrušit',
                        style: 'cancel',
                    },
                    {
                        text: 'Smazat', style: '', onPress: () => deleteMyself().then(result => {
                            if (result.status === 200) {
                                AsyncStorage.removeItem('token').then(() => {
                                    navigation.navigate('Unsigned');
                                }).catch(() => Error());
                            } else {
                                Error();
                            }
                        }).catch(() => Error()),
                    },
                ],
                {cancelable: false},
            );
        } else {
            Alert.alert(
                'Smazání mého účtu',
                'Opravdu chcete pokračovat?',
                [
                    {
                        text: 'Zrušit',
                        style: 'cancel',
                    },
                    {
                        text: 'Smazat', style: '', onPress: () => deleteMyself().then(result => {
                            if (result.status === 200) {
                                AsyncStorage.removeItem('token').then(() => {
                                    navigation.navigate('Unsigned');
                                }).catch(() => Error());
                            } else {
                                Error();
                            }
                        }).catch(() => Error()),
                    },
                ],
                {cancelable: false},
            );
        }
    };

    const submitOrg = () => {
        setOrg({
            ...org,
            submitted: true,
        });
        if (org.name.isValid) {
            saveOrganizationInfo({name: org.name.value}).then(result => {
                if (result.status === 200) {
                    ToastAndroid.showWithGravityAndOffset(
                        'Název organizace změněn',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    fetchOrgInfo().catch(() => Error());
                } else {
                    Error();
                }
            }).catch(() => Error());
        }
    };
    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <ScrollView>
                    <ProfileImage source={require('../../../../../resources/avatar.png')}/>
                    <ProfileFormView>
                        <InputView>
                            <FormTextInput
                                label='Jméno a příjmení'
                                value={form.username.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        username: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.username.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='E-mailová adresa'
                                value={form.email.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        email: {
                                            isValid: REG.test(text),
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.email.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Telefonní číslo'
                                value={form.phone.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        phone: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.phone.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Heslo'
                                value={form.password.value}
                                secureTextEntry={true}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        password: {
                                            isValid: text !== '' && text.length >= 6,
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.password.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Uložit změny</Button>
                    </ProfileFormView>
                    {role != null && role === Roles.Admin && <ProfileFormView style={{marginTop: 20}}>
                        <InputView>
                            <FormTextInput
                                label='Název organizace'
                                value={org.name.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setOrg({
                                        ...org,
                                        name: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {org.submitted && !org.name.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submitOrg}>Uložit změny</Button>
                    </ProfileFormView>}
                    <ProfileFormView>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={removeMyself}>Smazat můj
                                účet</BackButton>
                        </ButtonView>
                        <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                            AsyncStorage.removeItem('token').then(() => {
                                navigation.navigate('Unsigned');
                            }).catch();
                        }}>Odhlásit</BackButton>
                    </ProfileFormView>
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default User;

