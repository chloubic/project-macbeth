import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import RestaurantAdminInfo from './RestaurantAdminInfo';
import RestaurantAdminOpening from './RestaurantAdminOpening';
import RestaurantAdminGallery from './RestaurantAdminGallery';
import RestaurantAdminReservationOverview from './RestaurantAdminReservationOverview';
import MenuAdminOverview from './MenuAdminOverview';
import MenuAdminDetail from './MenuAdminDetail';
import Restaurants from './index';
import MenuAdminDetailInfo from './MenuAdminDetailInfo';
import RestaurantTables from './TablesAdmin';

let Stack = createStackNavigator();

const RestaurantAdminNavigation: () => React$Node = ({ route }) => {
    return (
        <Stack.Navigator headerMode={"none"} initialRouteName={"RestaurantAdminList"}>
            <Stack.Screen name="RestaurantAdminList" component={Restaurants}/>
            <Stack.Screen name="RestaurantAdminInfo" component={RestaurantAdminInfo}/>
            <Stack.Screen name="RestaurantAdminOpening" component={RestaurantAdminOpening}/>
            <Stack.Screen name="RestaurantAdminGallery" component={RestaurantAdminGallery}/>
            <Stack.Screen name="RestaurantAdminReservationOverview" component={RestaurantAdminReservationOverview}/>
            <Stack.Screen name="MenuAdminOverview" component={MenuAdminOverview}/>
            <Stack.Screen name="MenuAdminDetail" component={MenuAdminDetail}/>
            <Stack.Screen name="MenuAdminDetailInfo" component={MenuAdminDetailInfo}/>
            <Stack.Screen name="RestaurantTables" component={RestaurantTables}/>
        </Stack.Navigator>
    );
};

export default RestaurantAdminNavigation;

