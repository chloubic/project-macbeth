import {SafeAreaView, Text, ToastAndroid, View} from 'react-native';
import React, {useCallback, useState} from 'react';
import {MenuList, MenuTitle} from '../../../../../components/MenuOverview/styled';
import MenuAdminCategoryItem from '../../../../../components/MenuOverview/MenuAdminCategoryItem';
import {useFocusEffect} from '@react-navigation/native';
import getCategories from '../../../../../api/Menus/getCategories';
import Modal from 'react-native-modal';
import {ModalHeader, ModalView} from '../../../../../components/Modal/styled';
import {ProfileFormView} from '../../../../../components/Profile/Signed/User/styled';
import {BackButton, ButtonView, FormTextInput, InputView, ValidationText} from '../../../../../components/Form/styled';
import {Button} from 'react-native-paper';
import FloatingActionButton from 'react-native-floating-action-button';
import createCategory from '../../../../../api/Menus/createCategory';
import Error from '../../../../../api';
import Identity from '../../../../../api/Auth/Identity';
import AsyncStorage from '@react-native-community/async-storage';
import {ErrorText} from '../../../../../components/Common/styled';
import Loader from '../../../../../components/Common/loader';

const DEFAULT_FORM_VALS = {
    name: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const MenuAdminOverview: () => React$Node = ({route, navigation}) => {
    const [categories, setCategories] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const [error, setError] = useState(false);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [loading, setLoading] = useState(true);

    async function fetchCategories() {
        getCategories({id: route.params.id}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setLoading(false);
                    setCategories(json);
                }).catch(() => Error());
                setError(false);
            } else {
                setError(true);
                Error();
            }
        }).catch(() => Error());
    };
    useFocusEffect(
        useCallback(() => {
            setForm(DEFAULT_FORM_VALS);
            setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    fetchCategories().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, []),
    );

    const submit = () => {
        setForm({
            ...form,
            submitted: true,
        });
        if (form.name.isValid && categories != null) {
            createCategory({id: route.params.id, name: form.name.value, order: categories.length})
                .then(result => {
                    if (result.status === 200) {
                        ToastAndroid.showWithGravityAndOffset(
                            'Kategorie vytvořena',
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50,
                        );
                        fetchCategories().catch(() => Error());
                        setModalVisible(false);
                    } else { Error(); }
                }).catch(() => Error());
        }
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Dostupné kategorie</MenuTitle>
                <MenuList>
                    {categories == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {categories != null && categories.length === 0 && <ErrorText>Nenalezeny žádné kategorie</ErrorText>}
                    {categories != null && categories.map(item => {
                        return <MenuAdminCategoryItem key={item.id} navigation={navigation} id={item.id} name={item.name}/>;
                    })}
                </MenuList>
            </SafeAreaView>
            <Modal isVisible={modalVisible}>
                <ModalView>
                    <ModalHeader>Nová kategorie</ModalHeader>
                    <ProfileFormView>
                        <InputView>
                            <FormTextInput
                                label='Název kategorie'
                                value={form.name.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        name: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.name.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Vytvořit</Button>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                setModalVisible(false);
                            }}>Zavřít</BackButton>
                        </ButtonView>
                    </ProfileFormView>
                </ModalView>
            </Modal>
            <View style={{position: 'absolute', bottom: 50, right: 50}}>
                <FloatingActionButton
                    text="New"
                    size={60}
                    iconSize={30}
                    iconName="md-add"
                    iconType="Ionicons"
                    iconColor="#ffb200"
                    textDisable={true}
                    shadowColor="#ffb200"
                    rippleColor="#ffb200"
                    onPress={() => setModalVisible(!modalVisible)}
                />
            </View>
        </>
    );
};

export default MenuAdminOverview;
