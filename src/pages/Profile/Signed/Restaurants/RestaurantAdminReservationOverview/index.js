import {SafeAreaView, Text} from 'react-native';
import React, {useCallback, useState} from 'react';
import {MenuTitle} from '../../../../../components/MenuOverview/styled';
import {PhotoList} from '../../../../../components/Photogallery/styled';
import {useFocusEffect} from '@react-navigation/native';
import getReservationsById from '../../../../../api/Reservations/getReservationsById';
import ReservationItem from '../../../../../components/ReservationsOverview/reservationItem';
import Error from '../../../../../api';
import Identity from '../../../../../api/Auth/Identity';
import AsyncStorage from '@react-native-community/async-storage';
import {ErrorText} from '../../../../../components/Common/styled';
import Loader from '../../../../../components/Common/loader';

const RestaurantAdminReservationOverview: () => React$Node = ({ route }) => {
    const [reservations, setReservations] = useState(null);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);

    async function fetchReservations() {
        getReservationsById({id: route.params.id}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setLoading(false);
                    setReservations(json);
                }).catch(() => Error());
            } else { Error(); }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    fetchReservations().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, []),
    );
    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Rezervace</MenuTitle>
                <PhotoList>
                    {reservations == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {reservations != null && reservations.length === 0 && <ErrorText>Nenalezeny žádné rezervace</ErrorText>}
                    {reservations != null && reservations.map(item => {
                        return <ReservationItem key={item.id}
                                                userName={item.userName}
                                                userEmail={item.userEmail}
                                                userPhone={item.userPhone}
                                                table={item.table}
                                                date={item.date}
                                                from={item.from}
                                                to={item.to}
                        />;
                    })}
                </PhotoList>
            </SafeAreaView>
        </>
    );
};

export default RestaurantAdminReservationOverview;
