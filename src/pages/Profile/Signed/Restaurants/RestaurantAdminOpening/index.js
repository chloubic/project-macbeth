import {SafeAreaView, Text, ToastAndroid, View} from 'react-native';
import React, {useCallback, useState} from 'react';
import {MenuTitle} from '../../../../../components/MenuOverview/styled';
import {LabelText, PhotoList} from '../../../../../components/Photogallery/styled';
import Modal from 'react-native-modal';
import {ModalHeader, ModalView} from '../../../../../components/Modal/styled';
import {ProfileFormView} from '../../../../../components/Profile/Signed/User/styled';
import {BackButton, ButtonView, InputView, ValidationText} from '../../../../../components/Form/styled';
import {Button} from 'react-native-paper';
import RNPickerSelect from 'react-native-picker-select';
import FloatingActionButton from 'react-native-floating-action-button';
import {useFocusEffect} from '@react-navigation/native';
import getOpenings from '../../../../../api/Openings/getOpenings';
import getDays from '../../../../../api/Openings/getDays';
import OpeningItem from '../../../../../components/Openings';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Moment from 'react-moment';
import updateOpenings from '../../../../../api/Openings/updateOpenings';
import Error from '../../../../../api';
import Identity from '../../../../../api/Auth/Identity';
import AsyncStorage from '@react-native-community/async-storage';
import {ErrorText} from '../../../../../components/Common/styled';
import Loader from '../../../../../components/Common/loader';

const DEFAULT_FORM_VALS = {
    day: {
        value: '',
        isValid: false,
    },
    from: {
        value: new Date(),
        isValid: false,
    },
    to: {
        value: new Date(),
        isValid: false,
    },
    submitted: false,
};

const RestaurantAdminOpening: () => React$Node = ({route}) => {
    const [openings, setOpenings] = useState(null);
    const [days, setDays] = useState(null);
    const [dropdownDays, setDropdownDays] = useState(null);
    const [error, setError] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [isFromDatePickerVisible, setFromDatePickerVisibility] = useState(false);
    const [isToDatePickerVisible, setToDatePickerVisibility] = useState(false);
    const [loading, setLoading] = useState(true);

    async function fetchOpenings() {
        getOpenings({id: route.params.id}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setOpenings(json);
                    setLoading(false);
                }).catch(() => Error());
            } else { Error(); }
        }).catch(() => Error());
    }

    async function fetchDays() {
        getDays().then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setDays(json);
                    let data = [];
                    json.map(item => {
                        data.push({value: item.id, label: item.name});
                    });
                    setDropdownDays(data);
                }).catch(() => Error());
            } else { Error() }
        }).catch(() => Error());
    }
    useFocusEffect(
        useCallback(() => {
            setForm(DEFAULT_FORM_VALS);
            setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    fetchDays().catch(() => Error());
                    fetchOpenings().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, []),
    );

    const submit = () => {
        setForm({
            ...form,
            to: {
                value: form.to.value,
                isValid: form.from.value.getTime() < form.to.value.getTime()
            },
            submitted: true,
        });

        if (form.day.isValid && form.from.isValid && form.to.isValid && form.from.value.getTime() < form.to.value.getTime()) {
            let arr = openings;
            arr.push({
                dayId: form.day.value,
                from: form.from.value.toJSON(),
                to: form.to.value.toJSON()
            });
            updateOpenings({ id: route.params.id, openings: arr }).then(result => {
                if(result.status === 200){
                    ToastAndroid.showWithGravityAndOffset(
                        'Otevírací doba přidána',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    setOpenings(arr);
                    setModalVisible(false);
                    setForm(DEFAULT_FORM_VALS);
                }
                else { Error() }
            }).catch(() => Error());
        }
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Zadané otevírací doby</MenuTitle>
                <PhotoList>
                    {openings == null && days == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {openings != null && openings.length === 0 && <ErrorText>Nenalezeny žádné otevírací doby</ErrorText>}
                    {openings != null && days != null && openings.map((item, index) => {
                        return <OpeningItem key={index} id={route.params.id} index={index} dayId={item.dayId}
                                            from={item.from} to={item.to} days={days}
                                            openings={openings} setOpenings={setOpenings}/>;
                    })}
                </PhotoList>
            </SafeAreaView>
            <Modal isVisible={modalVisible}>
                <ModalView>
                    <ModalHeader>Nová otevírací doba</ModalHeader>
                    <ProfileFormView>
                        <InputView>
                            {dropdownDays != null && <RNPickerSelect
                                onValueChange={(value) => {
                                    if (value !== undefined) {
                                        setForm({
                                            ...form,
                                            day: {
                                                value: value,
                                                isValid: true,
                                            },
                                        });
                                    }
                                }}
                                placeholder={{label: 'Zvolte den v týdnu...'}}
                                items={dropdownDays}
                            />}
                            {form.submitted && !form.day.isValid &&
                            <ValidationText>Den je povinný!</ValidationText>}
                        </InputView>
                        <InputView>
                            <LabelText>Od</LabelText>
                            <Button color={'#ffb200'} labelStyle={{color: 'white'}} mode={'contained'}
                                    onPress={() => setFromDatePickerVisibility(true)}>
                                <Moment element={Text} format={"H:mm"} date={form.from.value}/>
                            </Button>
                            {form.submitted && !form.from.isValid &&
                            <ValidationText>Doba otevření je povinná, hodnota musí být nižší než doby
                                uzavření!</ValidationText>}
                        </InputView>
                        <InputView>
                            <LabelText>Do</LabelText>
                            <Button color={'#ffb200'} labelStyle={{color: 'white'}} mode={'contained'}
                                    onPress={() => setToDatePickerVisibility(true)}>
                                <Moment element={Text} format={"H:mm"} date={form.to.value}/>
                            </Button>
                            {form.submitted && !form.to.isValid &&
                            <ValidationText>Doba uzavření je povinná, hodnota musí být vyšší než doby
                                otevření!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Uložit</Button>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                setModalVisible(false);
                            }}>Zavřít</BackButton>
                        </ButtonView>
                    </ProfileFormView>
                </ModalView>
            </Modal>
            <DateTimePickerModal
                isVisible={isFromDatePickerVisible}
                mode="time"
                date={new Date()}
                onConfirm={selected => {
                    setFromDatePickerVisibility(false);
                    setForm({
                        ...form,
                        from: {
                            value: selected,
                            isValid: true,
                        },
                    });
                }}
                onCancel={() => setFromDatePickerVisibility(false)}
            />
            <DateTimePickerModal
                isVisible={isToDatePickerVisible}
                mode="time"
                date={new Date()}
                onConfirm={selected => {
                    setToDatePickerVisibility(false);
                    setForm({
                        ...form,
                        to: {
                            value: selected,
                            isValid: true,
                        },
                    });
                }}
                onCancel={() => setToDatePickerVisibility(false)}
            />
            {days != null && dropdownDays != null && <View style={{position: 'absolute', bottom: 50, right: 50}}>
                <FloatingActionButton
                    text="New"
                    size={60}
                    iconSize={30}
                    iconName="md-add"
                    iconType="Ionicons"
                    iconColor="#ffb200"
                    textDisable={true}
                    shadowColor="#ffb200"
                    rippleColor="#ffb200"
                    onPress={() => {
                        setForm(DEFAULT_FORM_VALS);
                        setModalVisible(!modalVisible);
                    }}
                />
            </View>}
        </>
    );
};

export default RestaurantAdminOpening;
