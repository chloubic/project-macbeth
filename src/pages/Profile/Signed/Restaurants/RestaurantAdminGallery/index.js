import {SafeAreaView, Text, ToastAndroid, View} from 'react-native';
import React, {useCallback, useState} from 'react';
import {MenuTitle} from '../../../../../components/MenuOverview/styled';
import {useFocusEffect} from '@react-navigation/native';
import getPhotogallery from '../../../../../api/Photogallery/getPhotogallery';
import PhotoItem from '../../../../../components/Photogallery';
import Modal from 'react-native-modal';
import {ModalHeader, ModalView} from '../../../../../components/Modal/styled';
import {ProfileFormView} from '../../../../../components/Profile/Signed/User/styled';
import {BackButton, ButtonView, InputView, ValidationText} from '../../../../../components/Form/styled';
import {RestaurantImage} from '../../../../../components/RestaurantList/styled';
import ImagePicker from 'react-native-image-picker';
import uploadImage from '../../../../../api/Image/uploadImage';
import {Button} from 'react-native-paper';
import FloatingActionButton from 'react-native-floating-action-button';
import updatePhotogallery from '../../../../../api/Photogallery/updatePhotogallery';
import {PhotoList} from '../../../../../components/Photogallery/styled';
import Error from '../../../../../api';
import Identity from '../../../../../api/Auth/Identity';
import AsyncStorage from '@react-native-community/async-storage';
import {ErrorText} from '../../../../../components/Common/styled';
import Loader from '../../../../../components/Common/loader';

const options = {
    title: 'Vyberte obrázek',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const DEFAULT_FORM_VALS = {
    image: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const RestaurantAdminGallery: () => React$Node = ({ route }) => {
    const [photos, setPhotos] = useState(null);
    const [ error, setError ] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [loading, setLoading] = useState(true);

    useFocusEffect(
        useCallback(() => {
            setForm(DEFAULT_FORM_VALS);
            setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    getPhotogallery({ id: route.params.id }).then(result => {
                        if(result.status === 200){
                            result.json().then(json => {
                                setLoading(false);
                                setPhotos(json);
                            }).catch(() => Error());
                            setError(false);
                        }
                        else {
                            setError(true);
                            Error();
                        }
                    }).catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, [])
    );

    const submit = () => {
        setForm({
            ...form,
            submitted: true
        });
        if(form.image.isValid){
            let arr = photos;
            arr.push(form.image.value);
            updatePhotogallery({ id: route.params.id, photos: arr }).then(result => {
                if(result.status === 200){
                    ToastAndroid.showWithGravityAndOffset(
                        'Obrázek přidán',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    setPhotos(arr);
                    setForm(DEFAULT_FORM_VALS);
                    setModalVisible(false);
                }
                else { Error(); }
            }).catch(() => Error());
        }
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Dostupné obrázky</MenuTitle>
                <PhotoList>
                    {photos == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {photos != null && photos.length === 0 && <ErrorText>Nenalezeny žádné obrázky</ErrorText>}
                    {photos != null && photos.map((item, index) => {
                        return <PhotoItem key={index} id={route.params.id} source={item} photos={photos} setPhotos={setPhotos} />;
                    })}
                </PhotoList>
            </SafeAreaView>
            <Modal isVisible={modalVisible}>
                <ModalView>
                    <ModalHeader>Nový obrázek</ModalHeader>
                    <ProfileFormView>
                        <InputView>
                            <RestaurantImage source={form.image.value !== "" ? {uri: form.image.value} : require('../../../../../../resources/avatar.png')}/>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                ImagePicker.launchImageLibrary(options, response => {
                                    if (response.uri) {
                                        uploadImage({ image: response }).then(result => {
                                            if(result.status === 201){
                                                setForm({
                                                    ...form,
                                                    image: {
                                                        value: result.headers.get("Location"),
                                                        isValid: true
                                                    }
                                                })
                                            }
                                            else { Error() }
                                        }).catch(() => Error());
                                    }
                                })
                            }}>Vybrat obrázek</BackButton>
                            {form.submitted && !form.image.isValid &&
                            <ValidationText>Obrázek je povinný!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Nahrát</Button>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                setModalVisible(false);
                            }}>Zavřít</BackButton>
                        </ButtonView>
                    </ProfileFormView>
                </ModalView>
            </Modal>
            <View style={{position:"absolute", bottom: 50, right: 50}}>
                <FloatingActionButton
                    text="New"
                    size={60}
                    iconSize={30}
                    iconName="md-add"
                    iconType="Ionicons"
                    iconColor="#ffb200"
                    textDisable={true}
                    shadowColor="#ffb200"
                    rippleColor="#ffb200"
                    onPress={() => setModalVisible(!modalVisible)}
                />
            </View>
        </>
    );
};

export default RestaurantAdminGallery;
