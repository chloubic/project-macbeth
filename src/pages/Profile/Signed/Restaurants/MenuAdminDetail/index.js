import {Alert, SafeAreaView, Text, ToastAndroid, View} from 'react-native';
import React, {useCallback, useState} from 'react';
import {MenuList, MenuTitle} from '../../../../../components/MenuOverview/styled';
import {useFocusEffect} from '@react-navigation/native';
import getMenu from '../../../../../api/Menus/getMenu';
import MenuAdminItem from '../../../../../components/MenuDetail/MenuAdminItem';
import Modal from 'react-native-modal';
import {ModalHeader, ModalView} from '../../../../../components/Modal/styled';
import {ProfileFormView} from '../../../../../components/Profile/Signed/User/styled';
import {BackButton, ButtonView, FormTextInput, InputView, ValidationText} from '../../../../../components/Form/styled';
import {Button} from 'react-native-paper';
import FloatingActionButton from 'react-native-floating-action-button';
import createItem from '../../../../../api/Menus/createItem';
import deleteCategory from '../../../../../api/Menus/deleteCategory';
import getCategoryInfo from '../../../../../api/Menus/getCategoryInfo';
import updateCategory from '../../../../../api/Menus/updateCategory';
import Error from '../../../../../api';
import Identity from '../../../../../api/Auth/Identity';
import AsyncStorage from '@react-native-community/async-storage';
import {ErrorText} from '../../../../../components/Common/styled';
import Loader from '../../../../../components/Common/loader';

const DEFAULT_CATEGORY_VALS = {
    name: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const DEFAULT_FORM_VALS = {
    name: {
        value: '',
        isValid: false,
    },
    description: {
        value: '',
        isValid: false,
    },
    weight: {
        value: '',
        isValid: false,
    },
    price: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const MenuAdminDetail: () => React$Node = ({route, navigation}) => {
    const [items, setItems] = useState(null);
    const [error, setError] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [category, setCategory] = useState(DEFAULT_CATEGORY_VALS);
    const [loading, setLoading] = useState(true);

    async function fetchItems() {
        getMenu({id: route.params.id}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setLoading(false);
                    setItems(json);
                }).catch(() => Error());
                setError(false);
            } else {
                setError(true);
                Error();
            }
        }).catch(() => Error());
    }

    async function fetchCategory() {
        getCategoryInfo({id: route.params.id}).then(result => {
            if(result.status === 200){
                result.json().then(json => {
                    setCategory({
                        name: {
                            value: json.name,
                            isValid: false,
                        },
                        submitted: false,
                    });
                }).catch(() => Error());
            }
            else { Error(); }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            setForm(DEFAULT_FORM_VALS);
            setCategory(DEFAULT_CATEGORY_VALS);
            setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    fetchItems().catch(() => Error());
                    fetchCategory().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, []),
    );

    const submit = () => {
        setForm({
            ...form,
            submitted: true,
        });
        if(form.name.isValid && form.description.isValid && form.weight.isValid && form.price.isValid && items != null){
            createItem({ id: route.params.id, name: form.name.value, description: form.description.value, weight: form.weight.value,
                price: form.price.value, order: items.length }).then(result => {
                if(result.status === 200) {
                    ToastAndroid.showWithGravityAndOffset(
                        'Položka vytvořena',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    fetchItems().catch(() => Error());
                    setModalVisible(false);
                }
                else { Error(); }
            }).catch(() => Error());
        }
    };

    const submitCategory = () => {
        setCategory({
            ...category,
            submitted: true,
        });
        if(category.name.isValid) {
            updateCategory({id: route.params.id, name: category.name.value}).then(result => {
                if(result.status === 200){
                    ToastAndroid.showWithGravityAndOffset(
                        'Položka vytvořena',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    fetchCategory().catch(() => Error());
                }
                else { Error(); }
            }).catch(() => Error());
        }
    };

    const removeCategory = () => {
        if(route.params.id != null){
            deleteCategory({id: route.params.id}).then(result => {
                if (result.status === 200) {
                    ToastAndroid.showWithGravityAndOffset(
                        'Kategorie odstraněna',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    navigation.navigate("MenuAdminOverview");
                } else Error();
            }).catch(() => Error());
        }
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Informace o kategorii</MenuTitle>
                <ProfileFormView style={{paddingBottom: 0}}>
                    <InputView>
                        <FormTextInput
                            label='Název'
                            value={category.name.value}
                            theme={{
                                colors: {
                                    primary: '#ffb200',
                                    underlineColor: 'transparent',
                                },
                            }}
                            onChangeText={text => {
                                setCategory({
                                    ...form,
                                    name: {
                                        isValid: text !== '',
                                        value: text,
                                    },
                                });
                            }}
                        />
                        {category.submitted && !category.name.isValid &&
                        <ValidationText>Toto pole je povinné!</ValidationText>}
                    </InputView>
                    <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                            onPress={submitCategory}>Uložit změny</Button>
                    <ButtonView>
                        <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                            Alert.alert(
                                'Smazání kategorie',
                                'Odstranění kategorie je nezvratný proces!',
                                [
                                    {
                                        text: 'Zrušit',
                                        style: 'cancel',
                                    },
                                    {
                                        text: 'Smazat', style: '', onPress: () => removeCategory(),
                                    },
                                ],
                                {cancelable: false},
                            );
                        }}>Smazat kategorii</BackButton>
                    </ButtonView>
                </ProfileFormView>
                <MenuTitle>Dostupné položky menu</MenuTitle>
                <MenuList>
                    {items == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {items != null && items.length === 0 && <ErrorText>Nenalezeny žádné položky</ErrorText>}
                    {items != null && items.map(item => {
                        return <MenuAdminItem key={item.id}
                                              navigation={navigation}
                                              id={item.id}
                                              name={item.name}
                                              description={item.description}
                                              price={item.price}
                                              weight={item.weight}/>;
                    })}
                </MenuList>
            </SafeAreaView>
            <Modal isVisible={modalVisible}>
                <ModalView>
                    <ModalHeader>Nová položka</ModalHeader>
                    <ProfileFormView>
                        <InputView>
                            <FormTextInput
                                label='Název položky'
                                value={form.name.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        name: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.name.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Popis položky'
                                value={form.description.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        description: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.description.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Hmotnost (např. 200 ml)'
                                value={form.weight.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        weight: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.weight.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Cena (např. 50,- Kč)'
                                value={form.price.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        price: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.price.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Vytvořit</Button>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                setModalVisible(false);
                            }}>Zavřít</BackButton>
                        </ButtonView>
                    </ProfileFormView>
                </ModalView>
            </Modal>
            <View style={{position: 'absolute', bottom: 50, right: 50}}>
                <FloatingActionButton
                    text="New"
                    size={60}
                    iconSize={30}
                    iconName="md-add"
                    iconType="Ionicons"
                    iconColor="#ffb200"
                    textDisable={true}
                    shadowColor="#ffb200"
                    rippleColor="#ffb200"
                    onPress={() => setModalVisible(!modalVisible)}
                />
            </View>
        </>
    );
};

export default MenuAdminDetail;
