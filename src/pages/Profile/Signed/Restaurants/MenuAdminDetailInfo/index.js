import {Alert, SafeAreaView, ToastAndroid} from 'react-native';
import React, {useCallback, useState} from 'react';
import {MenuTitle} from '../../../../../components/MenuOverview/styled';
import {useFocusEffect} from '@react-navigation/native';
import {ProfileFormView} from '../../../../../components/Profile/Signed/User/styled';
import {BackButton, ButtonView, FormTextInput, InputView, ValidationText} from '../../../../../components/Form/styled';
import {Button} from 'react-native-paper';
import getItemInfo from '../../../../../api/Menus/getItemInfo';
import updateItem from '../../../../../api/Menus/updateItem';
import deleteItem from '../../../../../api/Menus/deleteItem';
import Error from '../../../../../api';
import Identity from '../../../../../api/Auth/Identity';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from '../../../../../components/Common/loader';

const DEFAULT_FORM_VALS = {
    name: {
        value: '',
        isValid: true,
    },
    description: {
        value: '',
        isValid: true,
    },
    weight: {
        value: '',
        isValid: true,
    },
    price: {
        value: '',
        isValid: true,
    },
    order: 0,
    submitted: false,
};

const MenuAdminDetailInfo: () => React$Node = ({route, navigation}) => {
    const [error, setError] = useState(false);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [loading, setLoading] = useState(true);

    async function fetchItem() {
        getItemInfo({id: route.params.id}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setLoading(false);
                    setForm({
                        name: {
                            value: json.name,
                            isValid: true,
                        },
                        description: {
                            value: json.description,
                            isValid: true,
                        },
                        weight: {
                            value: json.weight,
                            isValid: true,
                        },
                        price: {
                            value: json.price,
                            isValid: true,
                        },
                        order: json.order,
                        submitted: false,
                    });
                }).catch(() => Error());
                setError(false);
            } else {
                setError(true);
                Error();
            }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            setLoading(true);
            setForm(DEFAULT_FORM_VALS);
            Identity().then(result => {
                if (result.isValid) {
                    fetchItem().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, []),
    );

    const submit = () => {
        setForm({
            ...form,
            submitted: true,
        });
        if(form.name.isValid && form.description.isValid && form.weight.isValid && form.price.isValid){
            updateItem({ id: route.params.id, name: form.name.value, description: form.description.value, weight: form.weight.value,
                price: form.price.value, order: form.order }).then(result => {
                if(result.status === 200) {
                    ToastAndroid.showWithGravityAndOffset(
                        'Položka aktualizována',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    fetchItem().catch(() => Error());
                }
                else { Error(); }
            }).catch(() => Error());
        }
    };

    const removeItem = () => {
        if(route.params.id != null){
            deleteItem({id: route.params.id}).then(result => {
                if (result.status === 200) {
                    ToastAndroid.showWithGravityAndOffset(
                        'Položka odstraněna',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    navigation.navigate("MenuAdminDetail");
                } else { Error(); }
            }).catch(() => Error());
        }
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Informace o položce</MenuTitle>
                <ProfileFormView style={{paddingBottom: 0}}>
                    <InputView>
                        <FormTextInput
                            label='Název'
                            value={form.name.value}
                            theme={{
                                colors: {
                                    primary: '#ffb200',
                                    underlineColor: 'transparent',
                                },
                            }}
                            onChangeText={text => {
                                setForm({
                                    ...form,
                                    name: {
                                        isValid: text !== '',
                                        value: text,
                                    },
                                });
                            }}
                        />
                        {form.submitted && !form.name.isValid &&
                        <ValidationText>Toto pole je povinné!</ValidationText>}
                    </InputView>
                    <InputView>
                        <FormTextInput
                            label='Popis'
                            value={form.description.value}
                            theme={{
                                colors: {
                                    primary: '#ffb200',
                                    underlineColor: 'transparent',
                                },
                            }}
                            onChangeText={text => {
                                setForm({
                                    ...form,
                                    description: {
                                        isValid: text !== '',
                                        value: text,
                                    },
                                });
                            }}
                        />
                        {form.submitted && !form.description.isValid &&
                        <ValidationText>Toto pole je povinné!</ValidationText>}
                    </InputView>
                    <InputView>
                        <FormTextInput
                            label='Hmotnost'
                            value={form.weight.value}
                            theme={{
                                colors: {
                                    primary: '#ffb200',
                                    underlineColor: 'transparent',
                                },
                            }}
                            onChangeText={text => {
                                setForm({
                                    ...form,
                                    weight: {
                                        isValid: text !== '',
                                        value: text,
                                    },
                                });
                            }}
                        />
                        {form.submitted && !form.weight.isValid &&
                        <ValidationText>Toto pole je povinné!</ValidationText>}
                    </InputView>
                    <InputView>
                        <FormTextInput
                            label='Cena'
                            value={form.price.value}
                            theme={{
                                colors: {
                                    primary: '#ffb200',
                                    underlineColor: 'transparent',
                                },
                            }}
                            onChangeText={text => {
                                setForm({
                                    ...form,
                                    price: {
                                        isValid: text !== '',
                                        value: text,
                                    },
                                });
                            }}
                        />
                        {form.submitted && !form.price.isValid &&
                        <ValidationText>Toto pole je povinné!</ValidationText>}
                    </InputView>
                    <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                            onPress={submit}>Uložit změny</Button>
                    <ButtonView>
                        <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                            Alert.alert(
                                'Smazání položky',
                                'Odstranění položky je nezvratný proces!',
                                [
                                    {
                                        text: 'Zrušit',
                                        style: 'cancel',
                                    },
                                    {
                                        text: 'Smazat', style: '', onPress: () => removeItem(),
                                    },
                                ],
                                {cancelable: false},
                            );
                        }}>Smazat položku</BackButton>
                    </ButtonView>
                </ProfileFormView>
            </SafeAreaView>
        </>
    );
};

export default MenuAdminDetailInfo;
