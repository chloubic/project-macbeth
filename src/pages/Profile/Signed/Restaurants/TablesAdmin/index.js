import {SafeAreaView, Text, ToastAndroid, View} from 'react-native';
import React, {useCallback, useState} from 'react';
import {MenuTitle} from '../../../../../components/MenuOverview/styled';
import {PhotoList} from '../../../../../components/Photogallery/styled';
import Modal from 'react-native-modal';
import {ModalHeader, ModalView} from '../../../../../components/Modal/styled';
import {ProfileFormView} from '../../../../../components/Profile/Signed/User/styled';
import {BackButton, ButtonView, FormTextInput, InputView, ValidationText} from '../../../../../components/Form/styled';
import {Button} from 'react-native-paper';
import FloatingActionButton from 'react-native-floating-action-button';
import {useFocusEffect} from '@react-navigation/native';
import getTablesById from '../../../../../api/Tables/getAll';
import createTable from '../../../../../api/Tables/createTable';
import TableItem from '../../../../../components/Tables';
import Error from '../../../../../api';
import Identity from '../../../../../api/Auth/Identity';
import AsyncStorage from '@react-native-community/async-storage';
import {ErrorText} from '../../../../../components/Common/styled';
import Loader from '../../../../../components/Common/loader';

const DEFAULT_FORM_VALS = {
    name: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const RestaurantTables: () => React$Node = ({route}) => {
    const [tables, setTables] = useState(null);
    const [error, setError] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [loading, setLoading] = useState(true);

    async function fetchTables() {
        getTablesById({id: route.params.id}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setLoading(false);
                    setTables(json);
                }).catch(() => Error());
            } else Error();
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            setForm(DEFAULT_FORM_VALS);
            setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    fetchTables().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, []),
    );

    const submit = () => {
        setForm({
            ...form,
            submitted: true,
        });

        if (form.name.isValid) {
            createTable({ id: route.params.id, name: form.name.value }).then(result => {
                if(result.status === 200){
                    ToastAndroid.showWithGravityAndOffset(
                        'Stůl k sezení přidán',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    fetchTables().catch(() => Error());
                    setModalVisible(false);
                    setForm(DEFAULT_FORM_VALS);
                }
                else { Error() }
            }).catch(() => Error());
        }
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Dostupné stoly k sezení</MenuTitle>
                <PhotoList>
                    {tables == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {tables != null && tables.length === 0 && <ErrorText>Nenalezeny žádné otevírací doby</ErrorText>}
                    {tables != null && tables.map((item, index) => {
                        return <TableItem key={index} id={item.id} name={item.name} tables={tables} setTables={setTables} />;
                    })}
                </PhotoList>
            </SafeAreaView>
            <Modal isVisible={modalVisible}>
                <ModalView>
                    <ModalHeader>Nový stůl k sezení</ModalHeader>
                    <ProfileFormView>
                        <InputView>
                            <FormTextInput
                                label='Název stolu'
                                value={form.name.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        name: {
                                            isValid: text !== "",
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.name.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Uložit</Button>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                setModalVisible(false);
                            }}>Zavřít</BackButton>
                        </ButtonView>
                    </ProfileFormView>
                </ModalView>
            </Modal>
            <View style={{position: 'absolute', bottom: 50, right: 50}}>
                <FloatingActionButton
                    text="New"
                    size={60}
                    iconSize={30}
                    iconName="md-add"
                    iconType="Ionicons"
                    iconColor="#ffb200"
                    textDisable={true}
                    shadowColor="#ffb200"
                    rippleColor="#ffb200"
                    onPress={() => {
                        setForm(DEFAULT_FORM_VALS);
                        setModalVisible(!modalVisible);
                    }}
                />
            </View>
        </>
    );
};

export default RestaurantTables;
