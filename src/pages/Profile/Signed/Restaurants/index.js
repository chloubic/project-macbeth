import React, {useCallback, useState} from 'react';
import {SafeAreaView, Text, ToastAndroid, View} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {RestaurantImage, RestaurantListView} from '../../../../components/RestaurantList/styled';
import Identity from '../../../../api/Auth/Identity';
import getRestaurantsByOrgId from '../../../../api/Restaurants/getRestaurantsByOrgId';
import FloatingActionButton from 'react-native-floating-action-button';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-picker';
import {Button} from 'react-native-paper';
import {ModalHeader, ModalView} from '../../../../components/Modal/styled';
import {ProfileFormView} from '../../../../components/Profile/Signed/User/styled';
import {BackButton, ButtonView, FormTextInput, InputView, ValidationText} from '../../../../components/Form/styled';
import uploadImage from '../../../../api/Image/uploadImage';
import createRestaurant from '../../../../api/Restaurants/createRestaurant';
import RestaurantAdminCard from '../../../../components/RestaurantList/CardItem/adminCard';
import Error from '../../../../api';
import {ErrorText} from '../../../../components/Common/styled';
import Loader from '../../../../components/Common/loader';

const options = {
    title: 'Vyberte hlavní obrázek',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const DEFAULT_FORM_VALS = {
    name: {
        value: '',
        isValid: false,
    },
    description: {
        value: '',
        isValid: true,
    },
    address: {
        value: '',
        isValid: false,
    },
    mainImage: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const Restaurants: () => React$Node = ({navigation}) => {
    const [data, setData] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [loading, setLoading] = useState(true);

    async function fetchRestaurants() {
        const identity = await Identity();
        getRestaurantsByOrgId({id: identity.organizationId}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setLoading(false);
                    setData(json);
                }).catch(() => Error());
            } else { Error(); }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            setLoading(true);
            setForm(DEFAULT_FORM_VALS);
            fetchRestaurants().catch(() => Error());
        }, []),
    );

    const submit = () => {
        setForm({
        ...form,
        submitted: true,
    });
        if (form.name.isValid && form.description.isValid && form.address.isValid && form.mainImage.isValid) {
            createRestaurant({
                name: form.name.value,
                description: form.description.value,
                address: form.address.value,
                mainImage: form.mainImage.value,
            })
                .then(result => {
                    if (result.status === 200) {
                        ToastAndroid.showWithGravityAndOffset(
                            'Restaurace vytvořena',
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50,
                        );
                        fetchRestaurants().catch(() => Error());
                        setModalVisible(false);
                        setForm(DEFAULT_FORM_VALS);
                    } else {
                        Error()
                    }
                }).catch(() => Error());
        }
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <RestaurantListView contentInsetAdjustmentBehavior="automatic">
                    {data != null && data.map(item => {
                        return <RestaurantAdminCard navigation={navigation} key={item.id} id={item.id} name={item.name}
                                               address={item.address}
                                               image={item.mainImage} status={item.status}/>;
                    })}
                    {data != null && data.length === 0 && <ErrorText>Žádné restaurace nebyly nalezeny</ErrorText>}
                </RestaurantListView>
            </SafeAreaView>
            <Modal isVisible={modalVisible}>
                <ModalView>
                    <ModalHeader>Nová restaurace</ModalHeader>
                    <ProfileFormView>
                        <InputView>
                            <FormTextInput
                                label='Název'
                                value={form.name.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        name: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.name.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Popis'
                                value={form.description.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        description: {
                                            isValid: true,
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.description.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Adresa'
                                value={form.address.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        address: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.address.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <RestaurantImage source={form.mainImage.value !== "" ? {uri: form.mainImage.value} : require('../../../../../resources/avatar.png')}/>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                ImagePicker.launchImageLibrary(options, response => {
                                    if (response.uri) {
                                        uploadImage({ image: response }).then(result => {
                                            if(result.status === 201){
                                                setForm({
                                                    ...form,
                                                    mainImage: {
                                                        value: result.headers.get("Location"),
                                                        isValid: true
                                                    }
                                                })
                                            }
                                            else { Error(); }
                                        }).catch(() => Error());
                                    }
                                })
                            }}>Vybrat obrázek</BackButton>
                            {form.submitted && !form.mainImage.isValid &&
                            <ValidationText>Obrázek je povinný!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Vytvořit</Button>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                setModalVisible(false);
                            }}>Zavřít</BackButton>
                        </ButtonView>
                    </ProfileFormView>
                </ModalView>
            </Modal>
            <View style={{position:"absolute", bottom: 50, right: 50}}>
                <FloatingActionButton
                    text="New"
                    size={60}
                    iconSize={30}
                    iconName="md-add"
                    iconType="Ionicons"
                    iconColor="#ffb200"
                    textDisable={true}
                    shadowColor="#ffb200"
                    rippleColor="#ffb200"
                    onPress={() => setModalVisible(!modalVisible)}
                />
            </View>
        </>
    );
};

export default Restaurants;
