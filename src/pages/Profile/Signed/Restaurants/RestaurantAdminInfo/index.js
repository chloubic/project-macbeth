import {Alert, SafeAreaView, ScrollView, ToastAndroid} from 'react-native';
import React, {useCallback, useState} from 'react';
import {RestaurantDetailButtonView} from '../../../../../components/RestaurantDetail/styled';
import {Button} from 'react-native-paper';
import {MenuTitle} from '../../../../../components/MenuOverview/styled';
import {ProfileFormView} from '../../../../../components/Profile/Signed/User/styled';
import {BackButton, ButtonView, FormTextInput, InputView, ValidationText} from '../../../../../components/Form/styled';
import {RestaurantImage} from '../../../../../components/RestaurantList/styled';
import ImagePicker from 'react-native-image-picker';
import uploadImage from '../../../../../api/Image/uploadImage';
import Identity from '../../../../../api/Auth/Identity';
import {useFocusEffect} from '@react-navigation/native';
import getRestaurantDetail from '../../../../../api/Restaurants/getRestaurantDetail';
import Loader from '../../../../../components/Common/loader';
import updateRestaurant from '../../../../../api/Restaurants/updateRestaurant';
import deleteRestaurant from '../../../../../api/Restaurants/deleteRestaurant';
import Error from '../../../../../api';
import AsyncStorage from '@react-native-community/async-storage';

const options = {
    title: 'Vyberte hlavní obrázek',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const DEFAULT_FORM_VALS = {
    name: {
        value: '',
        isValid: false,
    },
    description: {
        value: '',
        isValid: true,
    },
    address: {
        value: '',
        isValid: false,
    },
    mainImage: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const RestaurantAdminInfo: () => React$Node = ({route, navigation}) => {
    const [id, setId] = useState(null);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [loading, setLoading] = useState(true);

    async function fetchRestaurantInfo() {
        getRestaurantDetail({id: route.params.id}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setId(json.id);
                    setForm({
                        name: {
                            value: json.name,
                            isValid: true,
                        },
                        description: {
                            value: json.description,
                            isValid: true,
                        },
                        address: {
                            value: json.address,
                            isValid: true,
                        },
                        mainImage: {
                            value: json.mainImage,
                            isValid: true,
                        },
                        submitted: false,
                    });
                    setLoading(false);
                }).catch(() => Error());
            } else { Error(); }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            setForm(DEFAULT_FORM_VALS);
            Identity().then(result => {
                if (result.isValid) {
                    fetchRestaurantInfo().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, []),
    );

    const submit = () => {
        setForm({
            ...form,
            submitted: true,
        });
        if (form.name.isValid && form.description.isValid && form.address.isValid && form.mainImage.isValid && id != null) {
            updateRestaurant({ id: id, name: form.name.value, description: form.description.value, address: form.address.value, mainImage: form.mainImage.value })
                .then(result => {
                    if (result.status === 200) {
                        ToastAndroid.showWithGravityAndOffset(
                            'Informace aktualizovány',
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50,
                        );
                        fetchRestaurantInfo().catch(() => Error());
                    } else { Error(); }
                }).catch(() => Error());
        }
    };

    const removeRestaurant = () => {
        if(id != null){
            deleteRestaurant({id: id}).then(result => {
                if (result.status === 200) {
                    ToastAndroid.showWithGravityAndOffset(
                        'Restaurace odstraněna',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                    navigation.navigate("RestaurantAdminList");
                } else { Error() }
            }).catch(() => Error());
        }
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <ScrollView>
                    <MenuTitle>Rozcestník</MenuTitle>
                    <RestaurantDetailButtonView>
                        <RestaurantDetailButtonView>
                            <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                    onPress={() => navigation.navigate('RestaurantAdminGallery', {id: route.params.id})}>Fotogalerie</Button>
                        </RestaurantDetailButtonView>
                        <RestaurantDetailButtonView>
                            <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                    onPress={() => navigation.navigate('RestaurantAdminReservationOverview', {id: route.params.id})}>Rezervace</Button>
                        </RestaurantDetailButtonView>
                        <RestaurantDetailButtonView>
                            <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                    onPress={() => navigation.navigate('RestaurantTables', {id: route.params.id})}>Stoly k sezení</Button>
                        </RestaurantDetailButtonView>
                        <RestaurantDetailButtonView>
                            <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                    onPress={() => navigation.navigate('MenuAdminOverview', {id: route.params.id})}>Nabídka</Button>
                        </RestaurantDetailButtonView>
                        <RestaurantDetailButtonView>
                            <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                    onPress={() => navigation.navigate('RestaurantAdminOpening', {id: route.params.id})}>Otevírací
                                doba</Button>
                        </RestaurantDetailButtonView>
                    </RestaurantDetailButtonView>
                    <MenuTitle>Informace</MenuTitle>
                    <ProfileFormView>
                        <InputView>
                            <FormTextInput
                                label='Název'
                                value={form.name.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        name: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.name.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Popis'
                                value={form.description.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        description: {
                                            isValid: true,
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.description.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Adresa'
                                value={form.address.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        address: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.address.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <RestaurantImage
                                source={form.mainImage.value !== '' ? {uri: form.mainImage.value} : require('../../../../../../resources/avatar.png')}/>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                ImagePicker.launchImageLibrary(options, response => {
                                    if (response.uri) {
                                        uploadImage({image: response}).then(result => {
                                            if (result.status === 201) {
                                                setForm({
                                                    ...form,
                                                    mainImage: {
                                                        value: result.headers.get('Location'),
                                                        isValid: true,
                                                    },
                                                });
                                            } else { Error(); }
                                        }).catch(() => Error());
                                    }
                                });
                            }}>Vybrat obrázek</BackButton>
                            {form.submitted && !form.mainImage.isValid &&
                            <ValidationText>Obrázek je povinný!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Uložit změny</Button>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                Alert.alert(
                                    'Smazání restaurace',
                                    'Odstranění restaurace je nezvratný proces!',
                                    [
                                        {
                                            text: 'Zrušit',
                                            style: 'cancel',
                                        },
                                        {
                                            text: 'Smazat', style: '', onPress: () => removeRestaurant(),
                                        },
                                    ],
                                    {cancelable: false},
                                );
                            }}>Smazat restauraci</BackButton>
                        </ButtonView>
                    </ProfileFormView>
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default RestaurantAdminInfo;
