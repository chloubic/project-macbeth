import React, {useCallback, useState} from 'react';
import {Alert, SafeAreaView, Text, ToastAndroid, View} from 'react-native';
import Identity from '../../../../api/Auth/Identity';
import {useFocusEffect} from '@react-navigation/native';
import getOperators from '../../../../api/Operators/getOperators';
import Modal from 'react-native-modal';
import FloatingActionButton from 'react-native-floating-action-button';
import {Button} from 'react-native-paper';
import {
    ModalHeader,
    ModalView,
    OperatorEmail,
    OperatorName,
    OperatorPhone, OperatorsList,
    OperatorView,
} from '../../../../components/Modal/styled';
import {ProfileFormView} from '../../../../components/Profile/Signed/User/styled';
import {
    BackButton,
    ButtonView,
    FormTextInput,
    InputView,
    ValidationText,
} from '../../../../components/Form/styled';
import AsyncStorage from '@react-native-community/async-storage';
import createOperator from '../../../../api/Operators/createOperator';
import deleteOperator from '../../../../api/Operators/deleteOperator';
import Error from '../../../../api';
import {ErrorText} from '../../../../components/Common/styled';
import Loader from '../../../../components/Common/loader';

const REG = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const DEFAULT_FORM_VALS = {
    username: {
        value: '',
        isValid: false,
    },
    email: {
        value: '',
        isValid: false,
    },
    phone: {
        value: '',
        isValid: false,
    },
    password: {
        value: '',
        isValid: false,
    },
    submitted: false,
};

const Operators: () => React$Node = ({navigation}) => {
    const [operators, setOperators] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    const [loading, setLoading] = useState(true);

    const showConfirmDialog = (id) => {
        Alert.alert(
            'Smazání operátora',
            'Jste si opravdu jistý touto akcí?',
            [
                {
                    text: 'Zrušit',
                    style: 'cancel',
                },
                {text: 'Smazat', style: '', onPress: () => removeOperator(id)},
            ],
            {cancelable: false},
        );
    };

    async function fetchOperators() {
        getOperators().then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setLoading(false);
                    setOperators(json);
                }).catch(() => Error());
            } else {
            }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            setForm(DEFAULT_FORM_VALS);
            setLoading(true);
            Identity().then(result => {
                if (result.isValid) {
                    fetchOperators().catch(() => Error());
                } else {
                    AsyncStorage.removeItem('token').then(res => {
                        navigation.navigate("Unsigned");
                    }).catch(() => navigation.navigate("Unsigned"));
                }
            }).catch(() => navigation.navigate("Unsigned"));
        }, []),
    );
    const submit = () => {
        setForm({
            ...form,
            submitted: true,
        });
        if (form.username.isValid && form.email.isValid && form.phone.isValid && form.password.isValid) {
            createOperator({
                name: form.username.value,
                email: form.email.value,
                phone: form.phone.value,
                password: form.password.value,
            })
                .then(result => {
                    if (result.status === 200) {
                        ToastAndroid.showWithGravityAndOffset(
                            'Účet vytvořen',
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50,
                        );
                        setModalVisible(false);
                        fetchOperators().catch(() => Error());
                    } else {
                        Error();
                    }
                }).catch(() => Error());
        }
    };
    const removeOperator = (id) => {
        deleteOperator({ id: id }).then(result => {
            if(result.status === 200) {
                ToastAndroid.showWithGravityAndOffset(
                    'Operátor smazán',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                );
                fetchOperators().catch(() => Error());
            }
            else {
                Error();
            }
        }).catch(() => Error());
    };
    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                {operators == null && <ErrorText>Načítám...</ErrorText>}
                <OperatorsList>
                    {operators != null && operators.map(item => {
                        return <OperatorView key={item.id}>
                            <OperatorName>{item.name}</OperatorName>
                            <OperatorEmail>{item.email}</OperatorEmail>
                            <OperatorPhone>{item.phoneNumber}</OperatorPhone>
                            <ButtonView>
                                <BackButton mode={'text'} labelStyle={{color: '#ffb200'}}
                                            onPress={() => showConfirmDialog(item.id)}>Smazat</BackButton>
                            </ButtonView>
                        </OperatorView>;
                    })}
                    {operators != null && operators.length === 0 && <ErrorText>Žádní operátoři nenalezeni</ErrorText>}
                </OperatorsList>
            </SafeAreaView>
            <Modal isVisible={modalVisible}>
                <ModalView>
                    <ModalHeader>Nový operátor</ModalHeader>
                    <ProfileFormView>
                        <InputView>
                            <FormTextInput
                                label='Jméno a příjmení'
                                value={form.username.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        username: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.username.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='E-mailová adresa'
                                value={form.email.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        email: {
                                            isValid: REG.test(text),
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.email.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Telefonní číslo'
                                value={form.phone.value}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        phone: {
                                            isValid: text !== '',
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.phone.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <InputView>
                            <FormTextInput
                                label='Heslo'
                                value={form.password.value}
                                secureTextEntry={true}
                                theme={{
                                    colors: {
                                        primary: '#ffb200',
                                        underlineColor: 'transparent',
                                    },
                                }}
                                onChangeText={text => {
                                    setForm({
                                        ...form,
                                        password: {
                                            isValid: text !== '' && text.length >= 6,
                                            value: text,
                                        },
                                    });
                                }}
                            />
                            {form.submitted && !form.password.isValid &&
                            <ValidationText>Toto pole je povinné!</ValidationText>}
                        </InputView>
                        <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                onPress={submit}>Vytvořit</Button>
                        <ButtonView>
                            <BackButton mode={'text'} labelStyle={{color: '#ffb200'}} onPress={() => {
                                setModalVisible(false);
                            }}>Zavřít</BackButton>
                        </ButtonView>
                    </ProfileFormView>
                </ModalView>
            </Modal>
            <View style={{position: 'absolute', bottom: 50, right: 50}}>
                <FloatingActionButton
                    text="New"
                    size={60}
                    iconSize={30}
                    iconName="md-add"
                    iconType="Ionicons"
                    iconColor="#ffb200"
                    textDisable={true}
                    shadowColor="#ffb200"
                    rippleColor="#ffb200"
                    onPress={() => setModalVisible(!modalVisible)}
                />
            </View>
        </>
    );
};

export default Operators;

