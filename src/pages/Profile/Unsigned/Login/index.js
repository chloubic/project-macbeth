import React, {useCallback, useState} from 'react';
import {ScrollView, Text} from 'react-native';
import {Button} from 'react-native-paper';
import {
    FormView,
    ValidationText,
    FormImage,
    InputView, FormTextInput,
} from '../../../../components/Form/styled';
import {useFocusEffect} from '@react-navigation/native';
import logIn from '../../../../api/Auth/logIn';
import AsyncStorage from '@react-native-community/async-storage';
import Error from '../../../../api';

const REG = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const DEFAULT_FORM_VALS = {
    user: {
        value: '',
        isValid: false,
    },
    password: {
        value: '',
        isValid: false,
    },
    submitted: false,
    unauth: false
};

const Login: () => React$Node = ({ navigation, route }) => {
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    useFocusEffect(
        useCallback(() => {
            setForm(DEFAULT_FORM_VALS);
        }, [setForm])
    );
    const submit = () => {
        setForm({
            ...form,
            submitted: true
        });
        if(form.user.isValid && form.password.isValid) {
            logIn({ user: form.user.value, password: form.password.value }).then(result => {
                if(result.status === 200){
                    result.json().then(json => {
                        AsyncStorage.setItem("token", json.token).then(res => {
                            navigation.navigate("Signed");
                        }).catch(() => Error()); // Možná chyba? ....
                    });
                    setForm(DEFAULT_FORM_VALS);
                }
                else {
                    setForm({
                        ...form,
                        unauth: true
                    });
                }
            }).catch(() => Error());
        }
    };
    return (
        <ScrollView>
            <FormView>
                <FormImage source={require('../../../../../resources/logo.png')}/>
                {form.unauth && <Text>Špatný e-mail nebo heslo</Text>}
                <InputView>
                    <FormTextInput
                        label='Uživatel'
                        value={form.user.value}
                        theme={{
                            colors: {
                                primary: '#ffb200',
                                underlineColor: 'transparent',
                            },
                        }}
                        onChangeText={text => {
                            setForm({
                                ...form,
                                user: {
                                    isValid: REG.test(text),
                                    value: text,
                                },
                            });
                        }}
                    />
                    {form.submitted && !form.user.isValid && <ValidationText>Toto pole je povinné!</ValidationText>}
                </InputView>
                <InputView>
                    <FormTextInput
                        label='Heslo'
                        secureTextEntry={true}
                        value={form.password.value}
                        theme={{
                            colors: {
                                primary: '#ffb200',
                                underlineColor: 'transparent',
                            },
                        }}
                        onChangeText={text => {
                            setForm({
                                ...form,
                                password: {
                                    isValid: text !== '',
                                    value: text,
                                },
                            });
                        }}
                    />
                    {form.submitted && !form.password.isValid && <ValidationText>Toto pole je povinné!</ValidationText>}
                </InputView>
                <Button mode={"contained"} color={'#ffb200'} labelStyle={{color: "white"}} onPress={submit}>Přihlásit</Button>
            </FormView>
        </ScrollView>
    );
};

export default Login;

