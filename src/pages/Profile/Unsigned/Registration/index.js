import React, {useCallback, useEffect, useState} from 'react';
import {ScrollView, Text, ToastAndroid} from 'react-native';
import {
    BackButton, ButtonView,
    FormImage,
    FormTextInput,
    FormView,
    InputView, RegistrationLabel,
    ValidationText,
} from '../../../../components/Form/styled';
import {Button} from 'react-native-paper';
import {useFocusEffect} from '@react-navigation/native';
import registerCustomer from '../../../../api/Auth/registerCustomer';
import registerAdmin from '../../../../api/Auth/registerAdmin';
import Error from '../../../../api';

const REG = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const DEFAULT_FORM_VALS = {
    username: {
        value: '',
        isValid: false,
    },
    email: {
        value: '',
        isValid: false,
    },
    phone: {
        value: '',
        isValid: false,
    },
    password: {
        value: '',
        isValid: false,
    },
    orgName: {
        value: '',
        isValid: false,
    },
    submitted: false,
    error: false,
};

const Registration: () => React$Node = () => {
    const [isCustomer, setIsCustomer] = useState(null);
    const [form, setForm] = useState(DEFAULT_FORM_VALS);
    useFocusEffect(
        useCallback(() => {
            setIsCustomer(null);
            setForm(DEFAULT_FORM_VALS);
        }, [setIsCustomer, setForm])
    );
    const submit = () => {
        setForm({
            ...form,
            submitted: true,
        });
        if (form.username.isValid && form.phone.isValid && form.email.isValid && form.password.isValid) {
            if(isCustomer){
                registerCustomer({ name: form.username.value, email: form.email.value, phone: form.phone.value, password: form.password.value }).then(result => {
                    if(result.status === 200){
                        ToastAndroid.showWithGravityAndOffset(
                            "Účet vytvořen",
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50
                        );
                        setForm(DEFAULT_FORM_VALS);
                        setIsCustomer(null);
                    }
                    else {
                        setForm({
                            ...form,
                            error: true
                        });
                    }
                }).catch(() => Error());
            }
            else if(form.orgName.isValid) {
                registerAdmin({ name: form.username.value, email: form.email.value, phone: form.phone.value, password: form.password.value, orgName: form.orgName.value }).then(result => {
                    if(result.status === 200){
                        ToastAndroid.showWithGravityAndOffset(
                            "Účet vytvořen",
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50
                        );
                        setForm(DEFAULT_FORM_VALS);
                        setIsCustomer(null);
                    }
                    else {
                        setForm({
                            ...form,
                            error: true
                        });
                    }
                }).catch(() => Error());
            }
        }
    };
    return (
        <ScrollView>
            {isCustomer == null && <FormView>
                <FormImage source={require('../../../../../resources/logo.png')}/>
                <RegistrationLabel>Zvolte typ účtu</RegistrationLabel>
                <ButtonView>
                    <Button mode={"contained"} color={'#ffb200'} labelStyle={{color: "white"}} onPress={() => setIsCustomer(true)}>Zákazník</Button>
                </ButtonView>
                <ButtonView>
                    <Button mode={"contained"} color={'#ffb200'} labelStyle={{color: "white"}} onPress={() => setIsCustomer(false)}>Provozovatel</Button>
                </ButtonView>
            </FormView>}
            {isCustomer != null && <FormView>
                <FormImage source={require('../../../../../resources/logo.png')}/>
                <RegistrationLabel>
                    {isCustomer ? 'Nový zákazník' : 'Nový provozovatel'}
                </RegistrationLabel>
                {form.error && <Text>E-mailová adresa již v systému existuje</Text>}
                <InputView>
                    <FormTextInput
                        label='Jméno a příjmení'
                        value={form.username.value}
                        theme={{
                            colors: {
                                primary: '#ffb200',
                                underlineColor: 'transparent',
                            },
                        }}
                        onChangeText={text => {
                            setForm({
                                ...form,
                                username: {
                                    isValid: text !== '',
                                    value: text,
                                },
                            });
                        }}
                    />
                    {form.submitted && !form.username.isValid && <ValidationText>Toto pole je povinné!</ValidationText>}
                </InputView>
                <InputView>
                    <FormTextInput
                        label='E-mailová adresa'
                        value={form.email.value}
                        theme={{
                            colors: {
                                primary: '#ffb200',
                                underlineColor: 'transparent',
                            },
                        }}
                        onChangeText={text => {
                            setForm({
                                ...form,
                                email: {
                                    isValid: REG.test(text),
                                    value: text,
                                },
                            });
                        }}
                    />
                    {form.submitted && !form.email.isValid && <ValidationText>Toto pole je povinné!</ValidationText>}
                </InputView>
                <InputView>
                    <FormTextInput
                        label='Telefonní číslo'
                        value={form.phone.value}
                        theme={{
                            colors: {
                                primary: '#ffb200',
                                underlineColor: 'transparent',
                            },
                        }}
                        onChangeText={text => {
                            setForm({
                                ...form,
                                phone: {
                                    isValid: text !== '',
                                    value: text,
                                },
                            });
                        }}
                    />
                    {form.submitted && !form.phone.isValid && <ValidationText>Toto pole je povinné!</ValidationText>}
                </InputView>
                <InputView>
                    <FormTextInput
                        label='Heslo'
                        value={form.password.value}
                        secureTextEntry={true}
                        theme={{
                            colors: {
                                primary: '#ffb200',
                                underlineColor: 'transparent',
                            },
                        }}
                        onChangeText={text => {
                            setForm({
                                ...form,
                                password: {
                                    isValid: text !== '' && text.length >= 6,
                                    value: text,
                                },
                            });
                        }}
                    />
                    {form.submitted && !form.password.isValid && <ValidationText>Toto pole je povinné!</ValidationText>}
                </InputView>
                {!isCustomer && <InputView>
                    <FormTextInput
                        label='Název organizace'
                        value={form.orgName.value}
                        theme={{
                            colors: {
                                primary: '#ffb200',
                                underlineColor: 'transparent',
                            },
                        }}
                        onChangeText={text => {
                            setForm({
                                ...form,
                                orgName: {
                                    isValid: text !== '',
                                    value: text,
                                },
                            });
                        }}
                    />
                    {form.submitted && !form.orgName.isValid && <ValidationText>Toto pole je povinné!</ValidationText>}
                </InputView>}
                <Button mode={"contained"} color={'#ffb200'} labelStyle={{color: "white"}} onPress={submit}>Registrovat</Button>
                <ButtonView>
                    <BackButton color={"#ffb200"} mode={"text"} title={'Zpět'} onPress={() => {
                        setIsCustomer(null);
                        setForm(DEFAULT_FORM_VALS);
                    }}>Zpět</BackButton>
                </ButtonView>
            </FormView>}
        </ScrollView>
    );
};

export default Registration;

