import React, {useCallback, useState} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Identity from '../../../api/Auth/Identity';
import Login from './Login';
import Registration from './Registration';
import Error from '../../../api';
import Loader from '../../../components/Common/loader';

const Tab = createMaterialTopTabNavigator();

const Unsigned: () => React$Node = ({ navigation }) => {
    const [loading, setLoading] = useState(true);
    useFocusEffect(
        useCallback(() => {
            setLoading(true);
            Identity().then(result => {
                if(result.isValid){
                    navigation.navigate("Signed");
                }
                else {
                    setLoading(false);
                    AsyncStorage.removeItem("token").catch(() => Error());
                }
            }).catch(() => Error());
        }, [])
    );
    return (
        <>
            <Loader visible={loading}/>
            <Tab.Navigator
                tabBarOptions={{
                    activeTintColor: '#ffffff',
                    labelStyle: {fontSize: 14, fontWeight: 'bold'},
                    indicatorStyle: {
                        backgroundColor: 'white',
                    },
                    style: {backgroundColor: '#ffb200', color: 'white'},
                }}>
                <Tab.Screen name="Login" component={Login} options={{tabBarLabel: 'Přihlášení'}}/>
                <Tab.Screen name="Registration" component={Registration} options={{tabBarLabel: 'Registrace'}}/>
            </Tab.Navigator>
        </>
    );
};

export default Unsigned;

