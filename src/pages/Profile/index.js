import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Signed from './Signed';
import Unsigned from './Unsigned';

let Stack = createStackNavigator();

const Profile: () => React$Node = () => {
    return (
        <>
            <Stack.Navigator headerMode={"none"} initialRouteName={"Unsigned"}>
                <Stack.Screen name="Signed" component={Signed}/>
                <Stack.Screen name="Unsigned" component={Unsigned}/>
            </Stack.Navigator>
        </>
    );
};

export default Profile;

