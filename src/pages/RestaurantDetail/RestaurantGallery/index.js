import React, {useCallback, useState} from 'react';
import {Linking, SafeAreaView, ScrollView, Text} from 'react-native';
import PhotoGrid from 'react-native-thumbnail-grid';
import {useFocusEffect} from '@react-navigation/native';
import getPhotogallery from '../../../api/Photogallery/getPhotogallery';
import Error from '../../../api';
import Loader from '../../../components/Common/loader';
import {ErrorText} from '../../../components/Common/styled';

const RestaurantGallery: () => React$Node = ({route, navigation}) => {
    const [photos, setPhotos] = useState(null);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);
    useFocusEffect(
        useCallback(() => {
            if(photos == null) setLoading(true);
            getPhotogallery({id: route.params.id}).then(result => {
                if (result.status === 200) {
                    result.json().then(json => {
                        setLoading(false);
                        setPhotos(json);
                    }).catch(() => Error());
                    setError(false);
                } else {
                    setError(true);
                    Error();
                }
            }).catch(() => Error());
        }, []),
    );
    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <ScrollView>
                    {photos == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {photos != null && photos.length === 0 && <ErrorText>Nenalezeny žádné obrázky</ErrorText>}
                    {photos != null && photos.length > 0 &&
                    <PhotoGrid source={photos} onPressImage={(event, source) => Linking.openURL(source)}/>}
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default RestaurantGallery;

