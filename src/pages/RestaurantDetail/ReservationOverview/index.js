import React, {useCallback, useState} from 'react';
import {SafeAreaView, Text} from 'react-native';
import {
    DateTimePickerView,
    ReservationDivider,
    ReservationTitle,
} from '../../../components/ReservationsOverview/styled';
import {Button} from 'react-native-paper';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {useFocusEffect} from '@react-navigation/native';
import getAvailable from '../../../api/Tables/getAvailable';
import {PhotoList} from '../../../components/Photogallery/styled';
import ReservationTableItem from '../../../components/Tables/reservationTableItem';
import createReservation from '../../../api/Reservations/createReservation';
import Identity from '../../../api/Auth/Identity';
import Error from '../../../api';
import Loader from '../../../components/Common/loader';
import {ErrorText} from '../../../components/Common/styled';
import Moment from 'react-moment';

const RestaurantReservationOverview: () => React$Node = ({route, navigation}) => {
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isTimeToPickerVisible, setTimeToPickerVisibility] = useState(false);
    const [dateTime, setDateTime] = useState(new Date());
    const [timeTo, setTimeTo] = useState(new Date());
    const [tables, setTables] = useState(null);
    const [error, setError] = useState(false);
    const [role, setRole] = useState(null);
    const [loading, setLoading] = useState(true);

    async function fetchTables(from, to) {
        getAvailable({id: route.params.id, date: from.toJSON(), from: from.toJSON(), to: to.toJSON()}).then(result => {
            if (result.status === 200) {
                result.json().then(json => {
                    setTables(json);
                    setLoading(false);
                }).catch(() => Error());
            } else {
                Error();
            }
        }).catch(() => Error());
    }

    useFocusEffect(
        useCallback(() => {
            if(tables == null) setLoading(true);
            const from = new Date();
            const to = new Date();
            setDateTime(from);
            setTimeTo(to);
            to.setHours(to.getHours() + 1);
            Identity().then(result => {
                fetchTables(from, to).catch(() => Error());
                if (result.isValid) {
                    setRole(result.role);
                } else {
                    setRole(null);
                }
            }).catch(() => Error());
        }, []),
    );

    const newReservation = (id) => {
        createReservation({
            id: id,
            date: dateTime.toJSON(),
            from: dateTime.toJSON(),
            to: timeTo.toJSON(),
        }).then(result => {
            if (result.status === 200) {
                navigation.navigate('Reservations');
            } else {
                Error();
            }
        }).catch(() => Error());
    };

    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <ReservationTitle>Datum a čas rezervace</ReservationTitle>
                <DateTimePickerView>
                    <Button color={'#ffb200'} labelStyle={{color: 'white'}} mode={'contained'}
                            onPress={() => setDatePickerVisibility(true)}>
                        <Moment element={Text} format={"Do MMMM YYYY H:mm"} date={dateTime}/> - <Moment element={Text} format={"H:mm"} date={timeTo}/>
                    </Button>
                </DateTimePickerView>
                <ReservationDivider/>

                <PhotoList>
                    {tables == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {tables != null && tables.length === 0 && <ErrorText>Nenalezeny žádné stoly</ErrorText>}
                    {tables != null && tables.map(item => {
                        return <ReservationTableItem key={item.id}
                                                     id={item.id}
                                                     name={item.name}
                                                     newReservation={newReservation}
                                                     role={role}
                        />;
                    })}
                </PhotoList>
            </SafeAreaView>

            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="datetime"
                onConfirm={selected => {
                    setDateTime(selected);
                    setDatePickerVisibility(false);
                    setTimeToPickerVisibility(true);
                }}
                onCancel={() => setDatePickerVisibility(false)}
            />
            <DateTimePickerModal
                isVisible={isTimeToPickerVisible}
                mode="time"
                onConfirm={selected => {
                    setTimeToPickerVisibility(false);
                    fetchTables(dateTime, timeTo).catch(() => Error());
                    setTimeTo(selected);
                }}
                onCancel={() => setTimeToPickerVisibility(false)}
            />
        </>
    );
};

export default RestaurantReservationOverview;

