import React, {useCallback, useState} from 'react';
import {SafeAreaView, Text} from 'react-native';
import {MenuList, MenuTitle} from '../../../components/MenuOverview/styled';
import MenuCategoryItem from '../../../components/MenuOverview/MenuCategoryItem';
import {useFocusEffect} from '@react-navigation/native';
import getCategories from '../../../api/Menus/getCategories';
import Error from '../../../api';
import Loader from '../../../components/Common/loader';
import {ErrorText} from '../../../components/Common/styled';

const MenuOverview: () => React$Node = ({route, navigation}) => {
    const [categories, setCategories] = useState(null);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);
    useFocusEffect(
        useCallback(() => {
            if(categories == null) setLoading(true);
            getCategories({id: route.params.id}).then(result => {
                if (result.status === 200) {
                    result.json().then(json => {
                        setLoading(false);
                        setCategories(json);
                    }).catch(() => Error());
                    setError(false);
                } else {
                    setError(true);
                    Error();
                }
            }).catch(() => Error());
        }, []),
    );
    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Vyberte kategorii</MenuTitle>
                <MenuList>
                    {categories == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {categories != null && categories.length === 0 && <ErrorText>Nenalezeny žádné kategorie</ErrorText>}
                    {categories != null && categories.map(item => {
                        return <MenuCategoryItem key={item.id} navigation={navigation} id={item.id} name={item.name}/>;
                    })}
                </MenuList>
            </SafeAreaView>
        </>
    );
};

export default MenuOverview;
