import React, {useCallback, useState} from 'react';
import {SafeAreaView, Text} from 'react-native';
import {MenuList, MenuTitle} from '../../../components/MenuOverview/styled';
import MenuItem from '../../../components/MenuDetail/MenuItem';
import {useFocusEffect} from '@react-navigation/native';
import getMenu from '../../../api/Menus/getMenu';
import Error from '../../../api';
import Loader from '../../../components/Common/loader';
import {ErrorText} from '../../../components/Common/styled';

const MenuDetail: () => React$Node = ({route}) => {
    const [items, setItems] = useState(null);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);
    useFocusEffect(
        useCallback(() => {
            if(items == null) setLoading(true);
            getMenu({id: route.params.id}).then(result => {
                if (result.status === 200) {
                    result.json().then(json => {
                        setLoading(false);
                        setItems(json);
                    }).catch(() => Error());
                    setError(false);
                } else {
                    setError(true);
                    Error();
                }
            }).catch(() => Error());
        }, []),
    );
    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                <MenuTitle>Položky menu</MenuTitle>
                <MenuList>
                    {items == null && !error && <ErrorText>Načítám...</ErrorText>}
                    {error && <ErrorText>Během načítání se objevila chyba!</ErrorText>}
                    {items != null && items.length === 0 && <ErrorText>Nenalezeny žádné položky</ErrorText>}
                    {items != null && items.map(item => {
                        return <MenuItem key={item.id}
                                         name={item.name}
                                         description={item.description}
                                         price={item.price}
                                         weight={item.weight}/>;
                    })}
                </MenuList>
            </SafeAreaView>
        </>
    );
};

export default MenuDetail;
