import React, {useCallback, useState} from 'react';
import {SafeAreaView, ScrollView, Text} from 'react-native';
import {
    RestaurantDetailAddress,
    RestaurantDetailButtonView,
    RestaurantDetailDescription,
    RestaurantDetailImage,
    RestaurantDetailName,
    RestaurantDetailOpening,
    RestaurantDetailOpenings,
    RestaurantDetailStatusClosed,
    RestaurantDetailStatusOpened,
} from '../../components/RestaurantDetail/styled';
import {Button} from 'react-native-paper';
import {useFocusEffect} from '@react-navigation/native';
import getRestaurantDetail from '../../api/Restaurants/getRestaurantDetail';
import Error from '../../api';
import Loader from '../../components/Common/loader';

const RestaurantDetail: () => React$Node = ({route, navigation}) => {
    const [data, setData] = useState(null);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);
    useFocusEffect(
        useCallback(() => {
            if(data == null) setLoading(true);
            getRestaurantDetail({id: route.params.id}).then(result => {
                if (result.status === 200) {
                    result.json().then(json => {
                        setLoading(false);
                        setData(json);
                    }).catch(() => Error());
                    setError(false);
                } else {
                    setError(true);
                    Error();
                }
            }).catch(() => Error());
        }, []),
    );
    return (
        <>
            <Loader visible={loading}/>
            <SafeAreaView>
                {data == null && !error && <Text>Načítám...</Text>}
                {error && <Text>Chyba při načítání!</Text>}
                {data != null && <ScrollView>
                    <RestaurantDetailImage
                        source={{uri: data.mainImage}}
                    />
                    <RestaurantDetailName>{data.name}</RestaurantDetailName>
                    <RestaurantDetailAddress>{data.address}</RestaurantDetailAddress>
                    {data.status && <RestaurantDetailStatusOpened>Otevřeno</RestaurantDetailStatusOpened>}
                    {!data.status && <RestaurantDetailStatusClosed>Zavřeno</RestaurantDetailStatusClosed>}
                    <RestaurantDetailOpenings>
                        {data.opening.map((item, index) => {
                            return <RestaurantDetailOpening key={index}>{item}</RestaurantDetailOpening>;
                        })}
                    </RestaurantDetailOpenings>
                    <RestaurantDetailButtonView>
                        <RestaurantDetailButtonView>
                            <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                    onPress={() => navigation.navigate('RestaurantGallery', {id: route.params.id})}>Fotogalerie</Button>
                        </RestaurantDetailButtonView>
                        <RestaurantDetailButtonView>
                            <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                    onPress={() => navigation.navigate('RestaurantReservationOverview', {id: route.params.id})}>Rezervace</Button>
                        </RestaurantDetailButtonView>
                        <RestaurantDetailButtonView>
                            <Button mode={'contained'} color={'#ffb200'} labelStyle={{color: 'white'}}
                                    onPress={() => navigation.navigate('MenuOverview', {id: route.params.id})}>Nabídka</Button>
                        </RestaurantDetailButtonView>
                    </RestaurantDetailButtonView>
                    <RestaurantDetailDescription>{data.description}</RestaurantDetailDescription>
                </ScrollView>}
            </SafeAreaView>
        </>
    );
};

export default RestaurantDetail;

