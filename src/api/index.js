import {Alert} from 'react-native';

const Error = () => {
    Alert.alert(
        'Nastala (ne)očekávaná chyba!',
        'Zkuste to prosím později nebo resetujte aplikaci',
        [{ text: 'OK', style: 'cancel' }],
        {cancelable: false},
    );
};

export default Error;
