import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const createOperator = async ({ name, email, phone, password }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Users/CreateOperator", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            name: name,
            email: email,
            phone: phone,
            password: password
        })
    }));
};

export default createOperator;
