import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getOperators = async () => {
    return (await fetch(CONSTANTS.baseUrl + "api/Users/Operators", {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        }
    }));
};

export default getOperators;
