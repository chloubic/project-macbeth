import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const updatePhotogallery = async ({ id, photos }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Photogallery/Update/" + id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            urls: photos
        })
    }));
};

export default updatePhotogallery;
