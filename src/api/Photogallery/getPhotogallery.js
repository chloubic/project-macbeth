import CONSTANTS from '../../../app.config';

const getPhotogallery = async ({ id }) => {
    return await fetch(CONSTANTS.baseUrl + "api/Photogallery/" + id);
};

export default getPhotogallery;
