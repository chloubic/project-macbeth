import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const updateCategory = async ({ id, name }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Menu/Categories", {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            id: id,
            name: name
        })
    }));
};

export default updateCategory;
