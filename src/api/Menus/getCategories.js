import CONSTANTS from '../../../app.config';

const getCategories = async ({ id }) => {
    return await fetch(CONSTANTS.baseUrl + "api/Menu/Categories/" + id);
};

export default getCategories;
