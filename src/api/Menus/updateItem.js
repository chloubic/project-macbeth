import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const updateItem = async ({ id, name, description, weight, price, order}) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Menu/Items", {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            id: id,
            name: name,
            description: description,
            weight: weight,
            price: price,
            order: order
        })
    }));
};

export default updateItem;
