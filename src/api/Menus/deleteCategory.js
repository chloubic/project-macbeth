import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const deleteCategory = async ({ id }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Menu/Category/" + id, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        }
    }));
};

export default deleteCategory;
