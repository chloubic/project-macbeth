import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const createCategory = async ({ id, name, order }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Menu/Categories/" + id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            name: name,
            order: order
        })
    }));
};

export default createCategory;
