import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const createItem = async ({ id, name, description, weight, price, order }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Menu/Items/" + id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            name: name,
            description: description,
            weight: weight,
            price: price,
            order: order
        })
    }));
};

export default createItem;
