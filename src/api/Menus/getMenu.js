import CONSTANTS from '../../../app.config';

const getMenu = async ({ id }) => {
    return await fetch(CONSTANTS.baseUrl + "api/Menu/Category/" + id);
};

export default getMenu;
