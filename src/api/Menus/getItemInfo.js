import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getItemInfo = async ({id}) => {
    return await fetch(CONSTANTS.baseUrl + 'api/Menu/Item/Info/' + id, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
    });
};

export default getItemInfo;
