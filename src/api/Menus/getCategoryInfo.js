import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getCategoryInfo = async ({id}) => {
    return await fetch(CONSTANTS.baseUrl + 'api/Menu/Category/Info/' + id, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
    });
};

export default getCategoryInfo;
