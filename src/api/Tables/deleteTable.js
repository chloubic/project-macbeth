import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const deleteTable = async ({ id }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Table/" + id, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        }
    }));
};

export default deleteTable;
