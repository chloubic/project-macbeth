import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const createTable = async ({ id, name }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Table/" + id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            name: name
        })
    }));
};

export default createTable;
