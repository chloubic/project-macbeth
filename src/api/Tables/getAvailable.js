import CONSTANTS from '../../../app.config';

const getAvailable = async ({ id, date, from, to }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Table/Available/" + id + "?Date=" + date + "&From=" + from + "&To=" + to, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        },
    }));
};

export default getAvailable;
