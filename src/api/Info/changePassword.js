import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const changePassword = async ({ password }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Auth/ChangePassword", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            oldPassword: "",
            newPassword: password
        })
    }));
};

export default changePassword;
