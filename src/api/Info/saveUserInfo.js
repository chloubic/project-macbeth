import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const saveUserInfo = async ({ name, email, phone }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Users/Info", {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            name: name,
            email: email,
            phoneNumber: phone
        })
    }));
};

export default saveUserInfo;
