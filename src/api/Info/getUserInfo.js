import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getUserInfo = async () => {
    return (await fetch(CONSTANTS.baseUrl + "api/Users/Info", {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        }
    }));
};

export default getUserInfo;
