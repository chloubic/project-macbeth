import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const saveOrganizationInfo = async ({ name }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Organizations/Update", {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            name: name
        })
    }));
};

export default saveOrganizationInfo;
