import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getOrganizationInfo = async () => {
    return (await fetch(CONSTANTS.baseUrl + "api/Organizations/", {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        }
    }));
};

export default getOrganizationInfo;
