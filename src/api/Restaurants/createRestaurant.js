import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const createRestaurant = async ({ name, description, address, mainImage}) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Restaurants/Create", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            name: name,
            description: description,
            address: address,
            mainImage: mainImage
        })
    }));
};

export default createRestaurant;
