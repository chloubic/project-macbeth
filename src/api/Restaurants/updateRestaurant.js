import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const updateRestaurant = async ({ id, name, description, address, mainImage}) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Restaurants/Update", {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            id: id,
            name: name,
            description: description,
            address: address,
            mainImage: mainImage
        })
    }));
};

export default updateRestaurant;
