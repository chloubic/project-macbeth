import CONSTANTS from '../../../app.config';

const getRestaurantDetail = async ({ id }) => {
    return await fetch(CONSTANTS.baseUrl + "api/Restaurants/Detail/" + id);
};

export default getRestaurantDetail;
