import CONSTANTS from '../../../app.config';

const getRestaurants = async ({page, descOrder = true, search = null}) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Restaurants"));
};

export default getRestaurants;
