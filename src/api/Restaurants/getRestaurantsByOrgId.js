import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getRestaurantsByOrgId = async ({ id }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Restaurants/ByOrganization/" + id, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
    }));
};

export default getRestaurantsByOrgId;
