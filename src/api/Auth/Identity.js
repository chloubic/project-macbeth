import jwt from 'react-native-pure-jwt';
import AsyncStorage from '@react-native-community/async-storage';

const Identity = async () => {
    const token = await AsyncStorage.getItem("token");
    if(token != null)
    {
        const decoded = (await jwt.decode(token, "JustATestKeyJustATestKey", { complete: true, skipValidation: true }));
        return {
            organizationId: decoded.payload.OrganizationId,
            aud: decoded.payload.aud,
            exp: decoded.payload.exp,
            role: decoded.payload["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"],
            email: decoded.payload["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"],
            name: decoded.payload["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"],
            id: decoded.payload["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"],
            iss: decoded.payload.iss,
            nbf: decoded.payload.nbf,
            isValid: decoded.payload.exp > (Date.now() / 1000)
        };
    }
    return { isValid: false }
};

export default Identity;
