import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const deleteMyself = async () => {
    return (await fetch(CONSTANTS.baseUrl + "api/Users/DeleteMyself", {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        }
    }));
};

export default deleteMyself;
