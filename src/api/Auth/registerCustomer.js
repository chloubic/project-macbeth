import CONSTANTS from '../../../app.config';

const registerCustomer = async ({ name, email, phone, password }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Users/CreateClient", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            name: name,
            email: email,
            phone: phone,
            password: password
        })
    }));
};

export default registerCustomer;
