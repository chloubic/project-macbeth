import CONSTANTS from '../../../app.config';

const logIn = async ({ user, password }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Auth/Login", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: user,
            password: password
        })
    }));
};

export default logIn;
