const Roles = {
    Admin: "Admin",
    Operator: "Operator",
    Client: "Client"
};

export default Roles;
