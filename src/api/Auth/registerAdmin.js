import CONSTANTS from '../../../app.config';

const registerAdmin = async ({ name, email, phone, password, orgName }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Users/CreateAdmin", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            name: name,
            email: email,
            phone: phone,
            password: password,
            organizationName: orgName
        })
    }));
};

export default registerAdmin;
