import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const uploadImage = async ({ image }) => {
    const object = new FormData();
    await object.append("file", {
        name: image.fileName,
        type: image.type,
        uri: Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
    });
    return (await fetch(CONSTANTS.baseUrl + "api/Image", {
        method: "POST",
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: object
    }));
};

export default uploadImage;
