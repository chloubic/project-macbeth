import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getClientReservations = async () => {
    return (await fetch(CONSTANTS.baseUrl + "api/Reservations/Client/", {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
    }));
};

export default getClientReservations;
