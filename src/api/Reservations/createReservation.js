import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const createReservation = async ({ id, date, from, to }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Reservations/" + id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify({
            date: date,
            from: from,
            to: to
        })
    }));
};

export default createReservation;
