import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getReservationsById = async ({ id }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Reservations/Restaurant/" + id, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
    }));
};

export default getReservationsById;
