import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getDays = async () => {
    return (await fetch(CONSTANTS.baseUrl + "api/Opening/Days", {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
    }));
};

export default getDays;
