import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const updateOpenings = async ({ id, openings }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Opening/Update/" + id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        body: JSON.stringify(openings)
    }));
};

export default updateOpenings;
