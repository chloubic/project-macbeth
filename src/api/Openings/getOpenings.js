import CONSTANTS from '../../../app.config';
import AsyncStorage from '@react-native-community/async-storage';

const getOpenings = async ({ id }) => {
    return (await fetch(CONSTANTS.baseUrl + "api/Opening/" + id, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
    }));
};

export default getOpenings;
